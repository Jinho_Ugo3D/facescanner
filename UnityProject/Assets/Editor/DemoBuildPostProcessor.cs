﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.iOS.Xcode;

public class DemoBuildPostProcessor  {

  		[PostProcessBuild(0)]
  		public static void OnPostProcessBuild(BuildTarget target, string path)
		{
      		if (target != BuildTarget.iOS) return;

			string buildName = Path.GetFileNameWithoutExtension(path);
      		string pbxprojPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

			PBXProject proj = new PBXProject();
			proj.ReadFromString(File.ReadAllText(pbxprojPath));

			string buildTarget = proj.TargetGuidByName("Unity-iPhone");
			DirectoryInfo projectParent = Directory.GetParent(Application.dataPath);
			char divider = Path.DirectorySeparatorChar;

			//SET BITCODE = NO
			proj.SetBuildProperty(
				buildTarget, "ENABLE_BITCODE", "NO"
			);


			File.WriteAllText(pbxprojPath, proj.WriteToString() );
			
		}


}
