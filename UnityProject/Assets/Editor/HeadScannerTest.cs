﻿using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using NUnit.Framework;
using Ugo3D;
public class HeadScannerTest {

	private Texture2D ReadPNGFile(string FilePath){
		Texture2D Tex2D;
		byte[] FileData;
		if (File.Exists(FilePath)){
			FileData = File.ReadAllBytes(FilePath);
			Tex2D = new Texture2D(2, 2);    // Create new "empty" texture
			if (Tex2D.LoadImage(FileData))  // Load the imagedata into the texture (size is set automatically)
				return Tex2D;               // If data = readable -> return texture
		}
		Debug.Log("No file in "+FilePath);
		return null;
	}

	private void SavePNGFile(byte[] data, int width, int height, string FilePath){
		Texture2D tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
		tex.SetPixels32(data.Select(item => new Color32(item, item, item, 255)).ToArray());
		byte[] byteArray = tex.EncodeToPNG();
		System.IO.File.WriteAllBytes(FilePath, byteArray);
	}

	private FaceScanner faceScanner;
	private HeadScanner headScanner;

	[Test]
	public void IntegrationTest() {

		faceScanner = new FaceScanner();
		headScanner = new HeadScanner();

		// Read test images. 
		string mainPath = Path.Combine(Application.dataPath, "Editor");
		Texture2D frontFaceTex = this.ReadPNGFile( Path.Combine(mainPath,"front_face.png") );
		Texture2D rightFaceTex = this.ReadPNGFile( Path.Combine(mainPath,"right_face.png") );

		int width = frontFaceTex.width;
		int height = frontFaceTex.height;

		// Generate ellipse mask of front face.
		byte[] frontHeadMask 
			= headScanner.GenerateEllipseMask(410, height-550, 310, 360, width, height);

		SavePNGFile(frontHeadMask, width, height, Path.Combine(Application.streamingAssetsPath, "init_front_mask.png") );
		Assert.AreEqual(frontHeadMask.Length, width*height);

		// Refine current mask of front face. 
		var pixelsTemp = frontFaceTex.GetPixels32();
		GCHandle frontPixelsHandle = GCHandle.Alloc(pixelsTemp, GCHandleType.Pinned);
		byte[] newFrontHeadMask 
			= headScanner.RefineMaskByGrabCut(frontPixelsHandle, width, height, frontHeadMask,7);
		

		// Generate ellipse mask of right face.
		byte[] rightHeadMask 
			= headScanner.GenerateEllipseMask(width/2, height/2,
											  310, 360,
											  rightFaceTex.width, rightFaceTex.height);
		SavePNGFile(rightHeadMask, width, height, Path.Combine(Application.streamingAssetsPath, "init_right_mask.png") );
		Assert.AreEqual(rightHeadMask.Length, rightFaceTex.width*rightFaceTex.height);
		
		// Refine mask of right face.
		var pixelsTemp2 = rightFaceTex.GetPixels32();
		GCHandle rightPixelsHandle = GCHandle.Alloc(pixelsTemp2, GCHandleType.Pinned);
		byte[] newRightHeadMask
			= headScanner.RefineMaskByGrabCut(rightPixelsHandle, rightFaceTex.width, rightFaceTex.height, rightHeadMask);

		// TEST
		SavePNGFile(newFrontHeadMask, width, height, Path.Combine(Application.streamingAssetsPath, "front_mask.png") );
		SavePNGFile(newRightHeadMask, width, height, Path.Combine(Application.streamingAssetsPath, "right_mask.png") );


		// Do 4D face. 
		faceScanner.InitFaceScanner();
		faceScanner.Do4DFaceProcess(pixelsTemp, width, height);
		faceScanner.SaveCurrentFaceData(Application.streamingAssetsPath, "4Dface");
		float[] modelviewMat = faceScanner.GetModelViewMatrix();
		float[] projectiveMat = faceScanner.GetProjectiveMatrix();
		Vector3[] faceLandmarkIn3D = faceScanner.GetFaceLandmarkLocationIn3D();
		Debug.Log("Finished 4D face");


		// Build 3D volume. 
		headScanner.BuildHeadVolume(newFrontHeadMask,
									newRightHeadMask, 
									width, height, 
									modelviewMat, projectiveMat, 
									Path.Combine(Application.streamingAssetsPath, "dump_volume.obj"),
									51,61);
		Debug.Log("Finished building 3D head.");

		// Release memory.
		rightPixelsHandle.Free();
		frontPixelsHandle.Free();
	}

	[Test]
	public void FaceMaskTest() {

		faceScanner = new FaceScanner();
		headScanner = new HeadScanner();

		// Read test images. 
		string mainPath = Path.Combine(Application.dataPath, "Editor");
		Texture2D frontFaceTex = this.ReadPNGFile( Path.Combine(mainPath,"front_face.png") );

		// Find face landmark.
		var pixelsTemp = frontFaceTex.GetPixels32();
		if( faceScanner.FindFaceLandmark(pixelsTemp, frontFaceTex.width, frontFaceTex.height) )
		{
			// Found the face.
			Debug.Log("Found the face in image.");

			// Get ellipse mast fit on face. 
			Vector2[] landmarkPoints = faceScanner.GetFaceLandmarkLocation();
			byte[] mask = headScanner.GenerateEllipseMaskFromFaceLandmark(
							landmarkPoints,
							frontFaceTex.width,
							frontFaceTex.height);
			
			// Refine current mask of front face. 
			GCHandle frontPixelsHandle 
				= GCHandle.Alloc(frontFaceTex.GetPixels32(), GCHandleType.Pinned);
			byte[] newFrontHeadMask 
				= headScanner.RefineMaskByGrabCut(
						frontPixelsHandle,
						frontFaceTex.width, frontFaceTex.height,
						mask,7);
			
			
			// Store mask image. 
			SavePNGFile(mask, frontFaceTex.width, frontFaceTex.height, 
						Path.Combine(Application.streamingAssetsPath, "basic_mask_test.png") );
			SavePNGFile(newFrontHeadMask, frontFaceTex.width, frontFaceTex.height, 
						Path.Combine(Application.streamingAssetsPath, "front_face_fitted_mask_test.png") );
			Assert.AreEqual(newFrontHeadMask.Length, frontFaceTex.width*frontFaceTex.height);

		}else
		{
			Debug.Log("No face in the image.");
			Assert.False(true);
		}

	

	}
}
