﻿using UnityEngine;
using System;
using System.IO;

using System.Collections;
using System.Runtime.InteropServices;


namespace Ugo3D{

	public class HeadScanner {


		public enum Mask
		{
			GC_BGD    = 0,  // background
			GC_FGD    = 1,  // foreground
			GC_PR_BGD = 2,  // most probably background
			GC_PR_FGD = 3,   // most probably foreground
			GC_USER_FGD = 4,  // Probably user painted foreground
			GC_USER_BGD = 5, // Probably user painted background 
		}



        #if UNITY_EDITOR
            public const string DLLNAME = "FaceScannerMacOS";
        #elif UNITY_IPHONE
            public const string DLLNAME = "__Internal";
        #endif

			[DllImport (DLLNAME)]
			private static extern void get_ellipse_mask(int cx, int cy, 
														int semi_major_axis, int semi_minor_axis, 
														int width, int height, 

														byte[] mask_image );
			[DllImport (DLLNAME)]
			private static extern void get_mask_from_face_landmark(float[] facial_landmark,
												int length_of_facial_landmark,
												int width, int height,
												byte[] mask_image,
												int tx, int ty,
												int face_width_comp, int face_height_comp,
												int face_tx, int face_ty,
												int head_width_comp, int head_height_comp,
												int head_tx, int head_ty,
												int neck_width_comp, int neck_height_comp );

			[DllImport (DLLNAME)]
			private static extern void get_side_mask_from_face_landmark(float[] facial_landmark,
												int length_of_facial_landmark,
												int width, int height,
												bool is_left_side,
												byte[] mask_image, 
												int face_width_comp, int face_height_comp,
												int face_tx, int face_ty,
												int head_width_comp, int head_height_comp,
												int head_tx, int head_ty,
												int neck_width_comp, int neck_height_comp );


			[DllImport (DLLNAME)]
			private static extern void refine_mask_by_grabcut(
										System.IntPtr webcam_texture, 
										int width, int height, int iter, 
										byte[] mask, 
										bool use_hand_painted_mask);

			[DllImport (DLLNAME)]
			private static extern bool build_head_volume(byte[] front_mask,
												byte[] right_mask,
												int width, int height,
												float[] modelview_mat,
												float[] projective_mat,
												string file_path,
												int lod_of_depth,
												int lod_of_top_circle
												);
			
			[DllImport (DLLNAME)]
			private static extern bool get_side_face_mask(System.IntPtr image_for_unity,
															int width, int height,
															byte[] mask_for_unity);
		
			[DllImport (DLLNAME)]
			private static extern void get_roi_of_mask_image_for_unity(System.IntPtr mask_for_unity, int width, int height,
										ref int rect_x, ref int  rect_y, ref int rect_width, ref int rect_height);


		public byte[] GenerateEllipseMask(int cx, int cy, 
										  int semiMajorAxis, int semiMinorAxis,
										  int width, int height)
		{

			byte[] maskTemp = new byte[width*height];
			get_ellipse_mask(cx, cy, semiMajorAxis, semiMinorAxis, width, height, maskTemp);

			byte[] mask = new byte[width*height];
			System.Array.Copy(maskTemp, mask, width*height);
			return mask;
		}

		public byte[] GenerateSideMaskFromFaceLandmark(Vector2[] faceLandMarkList,
										  int width, int height, bool isLeftSide,
										  int face_width_comp = 80, int face_height_comp = 20,
										  int face_tx = -90, int face_ty = 30,
										  int head_width_comp = 30, int head_height_comp = 90,
										  int head_tx = -90, int head_ty = -40,
										  int neck_width_comp = 10, int neck_height_comp = 400)
		{
			int length = faceLandMarkList.Length;
			float[] temp_array = new float[2*length];

			for( int i=0; i<length; i++)
			{
				temp_array[2*i] = faceLandMarkList[i].x;
				temp_array[2*i+1] = faceLandMarkList[i].y; 
			}

			byte[] maskTemp = new byte[width*height];

			if(isLeftSide)
			{
				get_side_mask_from_face_landmark(temp_array, length, width, height, isLeftSide, maskTemp,
										face_width_comp, face_height_comp,
										-face_tx, face_ty, 
										head_width_comp, head_height_comp,
										-head_tx, head_ty,
										neck_width_comp, neck_height_comp);

			}else
			{

				get_side_mask_from_face_landmark(temp_array, length, width, height, isLeftSide, maskTemp,
										face_width_comp, face_height_comp,
										face_tx, face_ty, 
										head_width_comp, head_height_comp,
										head_tx, head_ty,
										neck_width_comp, neck_height_comp);

			}

			byte[] mask = new byte[width*height];
			System.Array.Copy(maskTemp, mask, width*height);
			return mask; 
		}

		public byte[] GenerateEllipseMaskFromFaceLandmark(Vector2[] faceLandMarkList,
										  int width, int height,
										  int tx = 0, int ty = 0,
										  int face_width_comp = 0, int face_height_comp = 0,
										  int face_tx = 0, int face_ty = 0,
										  int head_width_comp = 0, int head_height_comp = 0,
										  int head_tx = 0, int head_ty = 0,
										  int neck_width_comp = 0, int neck_height_comp = 0)
		{
			int length = faceLandMarkList.Length;
			float[] temp_array = new float[2*length];

			for( int i=0; i<length; i++)
			{
				temp_array[2*i] = faceLandMarkList[i].x;
				temp_array[2*i+1] = faceLandMarkList[i].y;
			}

			byte[] maskTemp = new byte[width*height];
			get_mask_from_face_landmark(temp_array, length, width, height, maskTemp, 
										tx, ty,
										face_width_comp, face_height_comp, 
										face_tx, face_ty, 
										head_width_comp, head_height_comp,
										head_tx, head_ty, 
										neck_width_comp, neck_width_comp);

			byte[] mask = new byte[width*height];
			System.Array.Copy(maskTemp, mask, width*height);
			return mask;
		}

		public byte[] GetSideFaceMask(GCHandle imageHandle, int width, int height)
		{
			byte[] maskTemp = new byte[width*height];
			bool res = get_side_face_mask( imageHandle.AddrOfPinnedObject(), width, height, maskTemp);
			
			if(res)
				return maskTemp; 
			else
				return null;

		}

		public byte[] RefineMaskByGrabCut(GCHandle imageHandle, int width, int height, byte[] mask, 
										  int iter=5, bool useHandPaintedMask = false )
		{
			byte[] maskTemp = new byte[width*height];
			System.Array.Copy(mask, maskTemp, width*height);

			refine_mask_by_grabcut(imageHandle.AddrOfPinnedObject(), 
								   width, height, iter, maskTemp, useHandPaintedMask);
			return maskTemp;
		}


		public bool BuildHeadVolume(byte[] frontHeadMask, 
									byte[] rightHeadMask, 
									int width, int height, 
									float[] modelviewMat, 
									float[] projectiveMat,
									string filePath,
									int lod_of_depth = 101,
									int lod_of_top_circle = 33
									)
		{
			byte[] frontMaskTemp = new byte[width*height];
			byte[] rightMaskTemp = new byte[width*height];

			System.Array.Copy(frontHeadMask, frontMaskTemp, width*height);
			System.Array.Copy(rightHeadMask, rightMaskTemp, width*height);

			return build_head_volume(frontMaskTemp, rightMaskTemp, 
									width, height, 
									modelviewMat, projectiveMat, 
									filePath, 
									lod_of_depth,
									lod_of_top_circle);
		 }

		public Rect GetROIOfMaskImage(System.IntPtr maskImage, int width, int height)
		 {
		 	int rect_x = 0, rect_y = 0, rect_width = 0, rect_height = 0;
		 	get_roi_of_mask_image_for_unity(maskImage, width, height,
											ref rect_x, ref rect_y, ref rect_width, ref rect_height);
			
			Rect rect = new Rect(rect_x,height-(rect_y+rect_height),rect_width,rect_height);
			return rect;
		 }

	}


}