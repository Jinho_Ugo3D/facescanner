using UnityEngine;
using System;
using System.IO;

using System.Collections;
using System.Runtime.InteropServices;


// C++ Callback //
[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
public delegate void MyDelegate(string str);

namespace Ugo3D{
    public class FaceScanner{

        private int imageWidth;
        private int imageHeight;

        /*
		public static void CallBackFunction(string str)
		{
			Debug.LogError("C++: " + str);
		}
        */
        // Interop functions from C. 

        #if UNITY_EDITOR
            public const string DLLNAME = "FaceScannerMacOS";
        #elif UNITY_IPHONE
            public const string DLLNAME = "__Internal";
        #endif


        [DllImport (DLLNAME)]
        private static extern bool detect_and_track_face(System.IntPtr current_image, int width, int height);


        [DllImport (DLLNAME)]
        private static extern int init_face_scanner(string model_file, string mappings_file, 
            string contour_file, string landmarkdetector_file, 
            string facedetector_file, string profile_facedetector_file, 
            string  blendshapes_file, string edgetopology_file, string normals_file);


        [DllImport (DLLNAME)]
        private static extern bool capture_mesh_and_texture(string path, string filename);

        [DllImport (DLLNAME)]
        private static extern void clean_up_face_scanner();

        [DllImport (DLLNAME)]
        private static extern bool get_modelview_mat(float[] matBuffer);

        [DllImport (DLLNAME)]
        private static extern bool get_projective_mat(float[] matBuffer);

        [DllImport (DLLNAME)]
        private static extern System.IntPtr get_current_iso_map_texture(out int width, out int height);
        
        [DllImport (DLLNAME)]
        private static extern bool get_face_landmark_location( float[] verties_on_image,  ref int length_of_vertices_on_image);

        [DllImport (DLLNAME)]
        private static extern bool get_face_landmark_in_3D_on_4D_face_mesh( float[]  verties_on_mesh, ref int number_of_vertices_on_mesh);


        [DllImport (DLLNAME)]
        private static extern bool find_face_landmark(System.IntPtr current_image, int width, int height);


        [DllImport (DLLNAME)]
        private static extern  bool get_uv_of_vertex_for_unity( int index_of_vertex, ref float u, ref float v );


        [DllImport (DLLNAME)]
        private static extern bool quick_find_face_landmark(
                                        System.IntPtr prev_image_for_unity,
                                        System.IntPtr cur_image_for_unity,
										int width, int height, int window_size);
        
        [DllImport (DLLNAME)]
        private static extern bool find_face_rect(System.IntPtr cur_image_for_unity,
                        int width, int height,
                        ref int rect_x, ref int rect_y, ref int rect_width, ref int rect_height);

/*
		#if UNITY_IPHONE
		[DllImport ("__Internal")]
		#else
		[DllImport ("FaceScannerMacOS")]
		#endif
		public static extern void SetDebugFunction(IntPtr fp);
*/
		 
        public int InitFaceScanner(){
            
            string mainPath = Application.streamingAssetsPath;
            string modelFile = Path.Combine(mainPath, "sfm_shape_3448.bin");
            string mappingsFile = Path.Combine(mainPath, "ibug2did.txt");
            string contourFile = Path.Combine(mainPath, "model_contours.json");
            string landmarkdetectorFile = Path.Combine(mainPath, "face_landmarks_model_rcr_68.bin");
            string facedetectorFile = Path.Combine(mainPath, "haarcascade_frontalface_alt2.xml");
            string profileFacedetectorFile = Path.Combine(mainPath, "haarcascade_profileface.xml");
            string blendshapesFile = Path.Combine(mainPath, "expression_blendshapes_3448.bin");
            string edgetopologyFile = Path.Combine(mainPath, "sfm_3448_edge_topology.json");
			string normalsFile = Path.Combine(mainPath, "model_normals.obj");
           
            return init_face_scanner(
                modelFile, mappingsFile, 
                contourFile, landmarkdetectorFile,
                facedetectorFile, profileFacedetectorFile, 
                blendshapesFile, edgetopologyFile, normalsFile);
        }

        public bool Do4DFaceProcess(Color32[] currentImage, int width, int height){

            // Convert Color32[] to C pointer. 
            GCHandle pixelsHandle = GCHandle.Alloc(currentImage, GCHandleType.Pinned);
            
            // Do face detection and traking. If face detector found face, it'll track that face. 
            bool isReadyToGet3DData 
                = detect_and_track_face(pixelsHandle.AddrOfPinnedObject(), width, height);
            
            imageWidth = width;
            imageHeight = height;
            
            // Release image.
            pixelsHandle.Free();
            return isReadyToGet3DData;
        }

        public bool Do4DFaceProcessForThreading(GCHandle pixelsHandle, int width, int height){

            // Do face detection and traking. If face detector found face, it'll track that face. 
            bool isReadyToGet3DData 
                = detect_and_track_face(pixelsHandle.AddrOfPinnedObject(), width, height);
            
            imageWidth = width;
            imageHeight = height;

            // Release image.
            pixelsHandle.Free();
            return isReadyToGet3DData;
        }

        public bool FindFaceLandmark(Color32[] currentImage, int width, int height){

            // Convert Color32[] to C pointer. 
            GCHandle pixelsHandle = GCHandle.Alloc(currentImage, GCHandleType.Pinned);
            
            // Do face detection and traking. If face detector found face, it'll track that face. 
            return FindFaceLandmarkForThreading(pixelsHandle, width, height);
        }

        public bool FindFaceLandmarkForThreading(GCHandle pixelsHandle, int width, int height){

            // Do face detection and traking. If face detector found face, it'll track that face. 
            bool isReadyToGet3DData 
                = find_face_landmark(pixelsHandle.AddrOfPinnedObject(), width, height);
            
            imageWidth = width;
            imageHeight = height;

            // Release image.
            pixelsHandle.Free();
            return isReadyToGet3DData;
        }
        public bool QuickFindFaceLandmark(Color32[] previousImage, Color32[] currentImage, 
                                          int width, int height, 
                                          int windowSize){

            // Convert Color32[] to C pointer. 
            GCHandle curPixelsHandle = GCHandle.Alloc(currentImage, GCHandleType.Pinned);
            GCHandle prevPixelsHandle = GCHandle.Alloc(previousImage, GCHandleType.Pinned);

            // Do face detection and traking. If face detector found face, it'll track that face. 
            return QuickFindFaceLandmarkForThreading(prevPixelsHandle, curPixelsHandle, width, height, windowSize);
        }
       
        public bool QuickFindFaceLandmarkForThreading(GCHandle prevPixelsHandle,
                                                      GCHandle curPixelsHandle,
                                                      int width, int height, 
                                                      int windowSize){

            // Do face detection and traking. If face detector found face, it'll track that face. 
            bool isReadyToGet3DData 
                = quick_find_face_landmark(
                    prevPixelsHandle.AddrOfPinnedObject(),
                    curPixelsHandle.AddrOfPinnedObject(), 
                    width, height, windowSize);
            
            imageWidth = width;
            imageHeight = height;

            // Release image.
            prevPixelsHandle.Free();
            curPixelsHandle.Free();
            return isReadyToGet3DData;
        }

        public bool FindFaceRect(Color32[] currentImage,
                                 int width, int height, 
                                 out Rect faceRectangle )
        {
            GCHandle curPixelsHandle = GCHandle.Alloc(currentImage, GCHandleType.Pinned);
            bool foundFace = FindFaceRectForThreading(curPixelsHandle, width, height, out faceRectangle);
            return foundFace;
        }

        public bool FindFaceRectForThreading(GCHandle curPixelsHandle,
                                             int width, int height, 
                                             out Rect faceRectangle )
        {
            int rect_x=0, rect_y=0, rect_width=0, rect_height=0;

            bool foundFace = find_face_rect( curPixelsHandle.AddrOfPinnedObject(), 
                                             width, height,
                                             ref rect_x, ref rect_y, 
                                             ref rect_width, ref rect_height);
            // It's origin is (Top-left).
            faceRectangle = new Rect(rect_x, rect_y,rect_width, rect_height);
            return foundFace;
        }

        public bool SaveCurrentFaceData(String path, String fileName){
            bool res = capture_mesh_and_texture(path, fileName);
            return res;
        }

        public float[] GetModelViewMatrix(){

            float[] tempArray = new float[16];
            bool res = get_modelview_mat(tempArray);

            if(res)
                return tempArray;
            else
                return new float[16];
        }
        public float[] GetProjectiveMatrix(){

            float[] tempArray = new float[16];
            bool res = get_projective_mat(tempArray);

            if(res)
                return tempArray;
            else
                return new float[16];
        }


        public Texture2D GetCurrentIsoMappedTexture(){

            int width, height;
            System.IntPtr buffer = get_current_iso_map_texture(out width,out height);

            Debug.Log("Texture dimention = ("+width+", "+height+")");
            
            int bufferSize = width*height*4;
            byte[] texBuff = new byte[bufferSize];
            Marshal.Copy(buffer, texBuff, 0, bufferSize);
            Texture2D tex = new Texture2D(width, height, TextureFormat.BGRA32, false);
            tex.LoadRawTextureData(texBuff);
            return tex;
        }

        public Vector2[] GetFaceLandmarkLocationinTextureCoord(){

            float[] tempArray = new float[200];
            int resultVertLength = 0;
            bool res = get_face_landmark_location(tempArray, ref resultVertLength);
            
            if( !res )
                return null;

            Vector2[] resultVertices = new Vector2[resultVertLength];

            for( int i=0; i<resultVertLength; i++){
                resultVertices[i][0] = tempArray[2*i];
                resultVertices[i][1] = imageHeight - tempArray[2*i+1]; // Transform corrdinate for texture2D coordinate.
            }
            return resultVertices;
        }

        public Vector2[] GetFaceLandmarkLocation(){

            float[] tempArray = new float[200];
            int resultVertLength = 0;
            bool res = get_face_landmark_location(tempArray, ref resultVertLength);
            
            if( !res )
                return null;

            Vector2[] resultVertices = new Vector2[resultVertLength];

            for( int i=0; i<resultVertLength; i++){
                resultVertices[i][0] = tempArray[2*i];
                resultVertices[i][1] = tempArray[2*i+1];
            }
            return resultVertices;
        }
        public Vector3[] GetFaceLandmarkLocationIn3D(){

            float[] tempArray = new float[70*3];
            int resultVertLength = 0;
            bool res = get_face_landmark_in_3D_on_4D_face_mesh(tempArray, ref resultVertLength);
            
            if( !res )
                return null;

            Vector3[] resultVertices = new Vector3[resultVertLength];

            for( int i=0; i<resultVertLength; i++)
            {
                resultVertices[i][0] = tempArray[3*i+0];
                resultVertices[i][1] = tempArray[3*i+1];
                resultVertices[i][2] = tempArray[3*i+2];
            }
            return resultVertices;
        }
    
        public Vector2? GetUVOfVertex( int indexOfVertex )
        {
            float u = 0.0f, v=0.0f;
            bool res = get_uv_of_vertex_for_unity(indexOfVertex, ref u, ref v);

            if (res)
            {
                return new Vector2(u, v);
            }else
                return null;
        }

        public void ResetFaceScanner(){
            clean_up_face_scanner();
        }


        
    }

}
