using UnityEngine;
using System;
using System.IO;

using System.Collections;
using System.Runtime.InteropServices;


namespace Ugo3D{
    public class VideoTracker
    {
        
        #if UNITY_EDITOR
            public const string DLLNAME = "FaceScannerMacOS";
        #elif UNITY_IPHONE
            public const string DLLNAME = "__Internal";
        #endif
        [DllImport (DLLNAME)]
        private static extern bool unity_init_video_tracker(float distance_limit, int window_size);


        [DllImport (DLLNAME)]
        private static extern bool unity_track_face_landmark(int idx,
                                     System.IntPtr cur_img_for_unity,
                                     System.IntPtr prev_img_for_unity,
                                     int width, int height,
                                     float[] current_key_face_landmark,
                                     ref int number_of_face_landmark,
                                     ref float reliability,
                                     ref bool using_optical_flow
                                     );

        [DllImport (DLLNAME)]
        private static extern bool unity_destroy_video_tracker();


        private int windowSize;
        private float distanceLimit;
        public VideoTracker()
        {
        }

        
        public bool Initialize(float distanceLimit = 7.0f, int windowSize = 21)
        {
            bool res = unity_init_video_tracker(distanceLimit, windowSize);
            if(!res)
                Debug.Log("Fail to initialize video tracker!!! ");

            return res;
        }

        public bool Reset()
        {
            bool res = unity_destroy_video_tracker();
            if(!res)
                Debug.Log("Fail to destroy video tracker!!!  ");
            
            return res;
        }

        public  bool TrackFaceLandmark(int index, 
                                      Color32[] curImage, Color32[] prevImage, 
                                      int width, int height, 
                                      out Vector2[] trackedLandmark,
                                      out float reliability,
                                      out bool using_optical_flow
                                      )
        {
            // Convert Color32[] to C pointer. 
            GCHandle curPixelsHandle = GCHandle.Alloc(curImage, GCHandleType.Pinned);
            GCHandle prevPixelsHandle = GCHandle.Alloc(prevImage, GCHandleType.Pinned);

            // Track points in current frame. 
            float[] tempArray = new float[200];
            int landmarkLength = 0;
            reliability = 0.0f;
            using_optical_flow = false;
            bool res = unity_track_face_landmark( index,
                                                  curPixelsHandle.AddrOfPinnedObject(), 
                                                  prevPixelsHandle.AddrOfPinnedObject(), 
                                                  width, height,
                                                  tempArray, 
                                                  ref landmarkLength,
                                                  ref reliability,
                                                  ref using_optical_flow);

            curPixelsHandle.Free();
            prevPixelsHandle.Free();
            
            if (!res)
            {
                trackedLandmark = null;
                Debug.Log("No face in current frame!!! Chage frame.");
                return false;
            }

            trackedLandmark = new Vector2[landmarkLength];
            for(int i=0; i<landmarkLength; ++i)
            {
                trackedLandmark[i].x = tempArray[2*i+0];
                trackedLandmark[i].y = tempArray[2*i+1];
            }

            return true;
        }

    }

}

