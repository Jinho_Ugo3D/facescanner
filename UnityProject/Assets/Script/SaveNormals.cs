﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SaveNormals : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		Mesh mesh = GetComponent<MeshFilter> ().mesh;
		print (mesh.vertexCount);

		string buffer = "";
		for (int i = 0; i < mesh.vertexCount; i++) {
			Vector3 normal = mesh.normals [i];
			print (normal);
			buffer += "vn " + normal.x + " " + normal.y + " " + normal.z + "\n";
		}

		string file_path = Application.streamingAssetsPath + "/model_normals.obj";
		File.WriteAllText (file_path, buffer);
	}
}
