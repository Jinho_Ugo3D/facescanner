﻿//========================================================================================================================================================//
// OBJECTIFY 
//========================================================================================================================================================//
// Created By: Erik Larsson
// Email: erik@inhuman.tech
// Usage: Objectify.Load("Model.obj")

//========================================================================================================================================================//
using UnityEngine;
using System.Collections.Generic;
using System.IO;

//========================================================================================================================================================//
// VERTEX //
//========================================================================================================================================================//
public class Vertex : System.IComparable<Vertex>
{
    public int Position;
    public int UV;
    public int Normal;
    public string ID;
    public int HashCode;
    public override bool Equals(object obj) { return obj.GetHashCode() == HashCode; }
    public override int GetHashCode() { return HashCode; }
    public override string ToString() { return ID; }
    public int CompareTo(Vertex vertex) { return ID.CompareTo(vertex.ID); }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------//
    public Vertex(int pos, int uv, int normal)
    {
        Position = pos;
        UV = uv;
        Normal = normal;
        ID = "FaceVertex(Pos: " + pos.ToString("D6") + ", UV: " + UV.ToString("D6") + ", Normal: " + Normal.ToString("D6") + ")";
        HashCode = ID.GetHashCode();
    }
}

//========================================================================================================================================================//
// TRIANGLE //
//========================================================================================================================================================//
public class Triangle
{
    public Vertex[] Vertices = new Vertex[3];
    public override string ToString() { return Vertices[0] + ", " + Vertices[1] + ", " + Vertices[2]; }
}

//========================================================================================================================================================//
// OBJ LOADER //
//========================================================================================================================================================//
static public class Objectify
{
    //----------------------------------------------------------------------------------------------------------------------------------------------------//
    static public GameObject Load(string file, float scale = 1)
    {
        float timer = Time.realtimeSinceStartup;

        List<Vector3> Positions = new List<Vector3>();
        List<Vector2> UVs = new List<Vector2>();
        List<Vector3> Normals = new List<Vector3>();
        List<Vertex> Vertices = new List<Vertex>();
        List<Vertex> SplitVertices = new List<Vertex>();
        List<Triangle> Triangles = new List<Triangle>();

        // Read OBJ File //
        string obj_file = File.ReadAllText(file);
        string[] lines = obj_file.Split('\n');
        char[] split_char = new char[] {' '};
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].Length < 2) continue;
            string[] parts = lines[i].Split(split_char, System.StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 0)
            {
                // Position //
                if (parts[0] == "v") Positions.Add(new Vector3(-float.Parse(parts[1]) * scale, float.Parse(parts[2]) * scale, float.Parse(parts[3]) * scale));
                
                // UV //
                else if (parts[0] == "vt") UVs.Add(new Vector2(float.Parse(parts[1]), float.Parse(parts[2])));

                // Normal //
                else if (parts[0] == "vn") Normals.Add(new Vector3(-float.Parse(parts[1]), float.Parse(parts[2]), float.Parse(parts[3])));

                // Face //
                else if (parts[0] == "f")
                {
                    Triangle triangle = new Triangle();
                    Triangles.Add(triangle);
                    for (int j = 1; j < 4; j++)
                    {
                        bool vertex_found = false;
                        int vertex_index = -1;
                        string[] vertex_data = parts[j].Split('/');
                        Vertex vertex = new Vertex(int.Parse(vertex_data[0]) - 1, int.Parse(vertex_data[1]) - 1, 0);

                        // Split Vertex //
                        for (int k = 0; k < Vertices.Count; k++)
                        {
                            if (vertex.Position == Vertices[k].Position && (vertex.UV != Vertices[k].UV || vertex.Normal != Vertices[k].Normal))
                            {
                                vertex_index = SplitVertices.IndexOf(vertex);
                                if (vertex_index == -1) SplitVertices.Add(vertex);
                                vertex_found = true;
                                break;
                            }
                        }

                        if (!vertex_found)
                        {
                            vertex_index = Vertices.IndexOf(vertex);

                            // File Vertex //
                            if (vertex_index == -1) Vertices.Add(vertex);

                            // Existing Vertex //
                            else vertex = Vertices[vertex_index];
                        }

                        triangle.Vertices[j - 1] = vertex;
                    }
                }
            }
        }
    
        Vertices.Sort();
        Vertices.AddRange(SplitVertices);

        // Build Mesh //
        List<Vector3> mesh_positions = new List<Vector3>();
        List<Vector2> mesh_uvs = new List<Vector2>();
        List<Vector3> mesh_normals = new List<Vector3>();
        for (int i = 0; i < Vertices.Count; i++)
        {
            mesh_positions.Add(Positions[Vertices[i].Position]);
            mesh_uvs.Add(UVs[Vertices[i].UV]);
            //mesh_normals.Add(Normals[Vertices[i].Normal]);
        }

        List<int> mesh_triangles = new List<int>();
        for (int i = 0; i < Triangles.Count; i++)
        {
            int v1 = Vertices.IndexOf(Triangles[i].Vertices[0]);
            int v2 = Vertices.IndexOf(Triangles[i].Vertices[1]);
            int v3 = Vertices.IndexOf(Triangles[i].Vertices[2]);
            if (v1 == -1 || v2 == -1 || v3 == -1)
            {
                Debug.Log("Vertex Error: " + i);
            }
            else
            {
                mesh_triangles.Add(v1);
                mesh_triangles.Add(v3);
                mesh_triangles.Add(v2);
            }
        }

        // Build GameObject //
        string object_name = Path.GetFileName(file).Split('.')[0];
        GameObject go = new GameObject(object_name, typeof(MeshFilter), typeof(MeshRenderer));
        Mesh mesh = go.GetComponent<MeshFilter>().mesh;
        mesh.SetVertices(mesh_positions);
        mesh.SetUVs(0, mesh_uvs);
        //mesh.SetNormals(mesh_normals);
        mesh.SetTriangles(mesh_triangles, 0, true);

        Debug.Log("Triangles: " + Triangles.Count + ", Vertices: " + Vertices.Count + ", Split: " + SplitVertices.Count + ", Time: " + (Time.realtimeSinceStartup - timer));

        return go;
    }
}
