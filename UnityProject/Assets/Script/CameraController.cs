﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Ugo3D;
using System.IO;
using System.Runtime.InteropServices;

public class CameraController : MonoBehaviour
{
	public WebCamTexture camera;
	public Quaternion baseRotation;
	public Color32[] data;
	public GameObject faceObject;
	public Material faceMaterial;

	private bool isReadyToGet3DData;

	private bool showFlagOrNot;

	private FaceScanner faceScanner;

	private int requestedTextureWidth, requestedTextureHeight;
	private Text caption;
	private int frameCount; //Do 4DFace process in every 30 frames for speed issue. 

	private Vector3 touchPos;
	private Quaternion startRotation;
	private Quaternion lastRotation;

	public AspectRatioFitter imageFitter;
	// Use this for initialization
	void Start ()
	{
		Debug.developerConsoleVisible = true;

		Debug.Log("WebCamTexture()");
		camera = new WebCamTexture ();
		WebCamDevice[] devices = WebCamTexture.devices;

		//Assign front camera if exists. 
		if ( 1 != devices.Length ){
			Debug.Log("Found front camera.");
			camera.deviceName = devices[1].name;
			camera = new WebCamTexture(devices[1].name);
		}

		//Begin to capture the camera. 
		Debug.Log("Begin to capture the camera.");
		Renderer renderer = GetComponent<Renderer>();
		renderer.material.mainTexture = camera;
		baseRotation = transform.rotation;
		camera.Play();
		transform.rotation = baseRotation * Quaternion.AngleAxis(camera.videoRotationAngle,Vector3.back);

		//Init FaceScanenr.
		Debug.Log("Init facescanenr.");
		isReadyToGet3DData = false;
		showFlagOrNot = true;
		frameCount = 0;
		faceScanner = new FaceScanner();
		faceScanner.InitFaceScanner();
		
	}
		
	void Update()
	{
		if (Input.GetMouseButtonDown (0)) {
			touchPos = Input.mousePosition;
			if (faceObject != null)
				startRotation = faceObject.transform.rotation;
		}
		if (Input.GetMouseButton (0) && faceObject != null) {
			float rot = (Input.mousePosition.x - touchPos.x) * 1f;
			faceObject.transform.rotation = startRotation * Quaternion.Euler (0, rot, 0);
			lastRotation = faceObject.transform.rotation;
		}

		// Rotate image for mobile device.
		#if !UNITY_EDITOR
			transform.rotation = baseRotation * Quaternion.AngleAxis(camera.videoRotationAngle + 180, Vector3.forward);
		#endif

	}
		

	public Texture2D rotateTexture(Texture2D image )
	{

		Texture2D target = new Texture2D(image.height, image.width, image.format, false);    //flip image width<>height, as we rotated the image, it might be a rect. not a square image

		Color32[] pixels = image.GetPixels32(0);
		pixels = rotateTextureGrid(pixels, image.width, image.height);
		target.SetPixels32(pixels);
		target.Apply();

		//flip image width<>height, as we rotated the image, it might be a rect. not a square image

		return target;
	}


	public Color32[] rotateTextureGrid(Color32[] tex, int wid, int hi)
	{
		Color32[] ret = new Color32[wid * hi];      //reminder we are flipping these in the target

		for (int y = 0; y < hi; y++)
		{
			for (int x = 0; x < wid; x++)
			{
				ret[(hi-1)-y + x * hi] = tex[x + y * wid]; //CCW
				//ret[y + (wid-1-x) * hi] = tex[x + y * wid];  
			}
		}

		return ret;
	}
		
	public void onClick(){
		string filePath = Application.persistentDataPath  + "/";
		string fileName = "ugo3dface";
		string obj_path = filePath + fileName + ".obj";
		string texture_path = filePath + fileName + ".isomap.png";

		Color32[] imageData = camera.GetPixels32 ();

		// 4D Face //
		Debug.Log("4DFace");
		isReadyToGet3DData = faceScanner.Do4DFaceProcess(imageData, camera.width, camera.height);

		// Delete Previous //
		Debug.Log("Delete Previous");
		if(File.Exists(obj_path)) 
			File.Delete(obj_path);
		if(File.Exists(texture_path))
			File.Delete(texture_path);
		if (faceObject != null)
			DestroyImmediate (faceObject);

		if ( isReadyToGet3DData ){
			Debug.Log("Ready to Get 3D data.");

			if( faceScanner.SaveCurrentFaceData(filePath, fileName) && File.Exists(obj_path) ){
				Debug.Log("Stored data in "+filePath );

				// Load Model //
				faceObject = Objectify.Load(obj_path);
				faceObject.transform.rotation = lastRotation;

				// Load Texture //
				byte[] data = File.ReadAllBytes(texture_path);
			    Texture2D tex = new Texture2D(2, 2);
				tex.LoadImage(data);
				Material mat = new Material (faceMaterial);
				mat.SetTexture ("_MainTex", tex);
				faceObject.GetComponent<Renderer>().material = mat;

				// Get projective and modelview matrix. 
				float[] modelview = faceScanner.GetModelViewMatrix();
				Debug.Log("Modelview matrix = "+ modelview);
				
				float[] projective = faceScanner.GetProjectiveMatrix();
				Debug.Log("Projective matrix = "+ projective);

				// Get isomap texture. 
				Texture2D isomap = faceScanner.GetCurrentIsoMappedTexture();

			

				// Dump the data to debug in editor mode.. 
				#if UNITY_EDITOR
					// Dump isomap texture in streaming asset folder as test. 
					byte[] isomap_bytes = isomap.EncodeToPNG();					
					File.WriteAllBytes(
						Application.streamingAssetsPath +"/"+"current_iso_img.png",
						isomap_bytes);
					isomap_bytes = null;
				#endif


				// Remove temporary objects.
				Object.Destroy(isomap);

			}else{
				Debug.Log("Fail to store data.");
			} 
		}else{
			Debug.Log("Not ready to grap face ");
		}
	}


	public void Reset(){

		// Delete Old //
		if (faceObject != null)
			DestroyImmediate (faceObject);

		if (faceScanner != null){
			faceScanner.ResetFaceScanner();
			faceScanner = null;
			faceScanner = new FaceScanner();
			faceScanner.InitFaceScanner();
		}
	}

}