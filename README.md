FaceScanner
==================
TBD

FaceScanner
-----------
TBD

HeadScanner
-----------
TBD


VideoTracker
-----------
 Track the special key face landmarks like below.

 iBug face landmark annotation | Description
-------------------------------|-------------
 46 | left eye outer-corner (2)
 36 | left nostril, below nose, nose-lip junction
 55 | left mouth corner (13)
 37 | right eye outer-corner (1)
 32 | right nostril, below nose, nose-lip junction
 49 | right mouth corner (12)

  **WARNING**
  YOU HAVE TO INITIALIZE ```FaceScanner``` before construct ```VideoTracker``` instance. Wiil be fixed this issue ASAP.

### bool VideoTracker::VideoTracker( float distanceLimit = 7.0f, int windowSize = 21 )

 Initialize ```VideoTracker``` module.

* Parameters
    * distanceLimit : Limitation of feature tracking. (In pixel)
    * windowSize : Window size to search the next position in next frame by optical flow.

* Return
    * True : Ready to track.
    * False : Fail to initialize.


### bool VideoTracker::bool TrackFaceLandmark(int index, Color32[] curImage, Color32[] prevImage,int width, int height, out Vector2[] trackedLandmark)

 Track the key face landmarks in every frame.

 * Parameters
     * index : Index of video frame to distingush frame order.
     * curImage : Current video frame buffer in ```Texture2D``` format.
     * prevImage: Previous video frame buffer in ```Texture2D``` format.
     * width: Width of image
     * height: Height of image
     * trackedLandmark: Tracked key face landmarks. It stores 2D points in array like below.

   Index | Description
   -------------------------------|-------------
   0 | left eye outer-corner (2)
   1 | left nostril, below nose, nose-lip junction
   2 | left mouth corner (13)
   3 | right eye outer-corner (1)
   4 | right nostril, below nose, nose-lip junction
   5 | right mouth corner (12)



 * Return
     * True : In 1st frame (index =0), it means succeed to find the face in the image and ready to track.
     * False : Fail to track points. Reset tracker.