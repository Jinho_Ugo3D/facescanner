//
//  head_scanner.cpp
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 3. 18..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/ext.hpp"

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cassert>

#include <string.h>

#include "head_scanner.h"
#include "face_scanner.h"

#include "unity_util.h"

using cv::Mat;
using cv::Size;
using cv::Vec2f;
using cv::Vec3f;
using cv::Vec4f;
using cv::Scalar;
using cv::Rect;


cv::Rect rescale_rect(cv::Rect facebox, float scaling, float translation_y)
{
    // Assumes a square input facebox to work? (width==height)
    const auto new_width = facebox.width * scaling;
    const auto smaller_in_px = facebox.width - new_width;
    const auto new_tl = facebox.tl() + cv::Point2i(smaller_in_px / 2.0f, smaller_in_px / 2.0f);
    const auto new_br = facebox.br() - cv::Point2i(smaller_in_px / 2.0f, smaller_in_px / 2.0f);
    cv::Rect rescaled_facebox(new_tl, new_br);
    rescaled_facebox.y += facebox.width * translation_y;
    return rescaled_facebox;
};



bool get_head_silhouette_in_image(cv::Mat& webtexture_image, cv::CascadeClassifier& face_cascade, cv::Mat& fgmask)
{
       
    // Convert image in RGB format and left-top origin.
    cv::Mat rgb_frame = cv::Mat(webtexture_image.cols, webtexture_image.rows, CV_8UC3);
    cv::cvtColor(webtexture_image, rgb_frame, cv::COLOR_BGRA2RGB);
    cv::flip(rgb_frame, rgb_frame, 0);
    
    // Find the face in the image.
    cv::Mat half_rgb_frame = cv::Mat(webtexture_image.rows/2, webtexture_image.cols/2, CV_8UC3);
    cv::resize(rgb_frame,half_rgb_frame, half_rgb_frame.size() );
    std::vector<cv::Rect> detected_faces;
    face_cascade.detectMultiScale(half_rgb_frame, detected_faces, 1.2, 2, 0,
                                  cv::Size(110, 110) );
    
    if( detected_faces.empty() ){
        std::cout <<"Cannot find the face in the image." <<std::endl;
        return false;
    }
    else{
        cv::Rect new_face_rect = rescale_rect(detected_faces[0], 1.3, -0.05);
        
        // Dump for debug.
#ifdef CONTOUR_DEBUG
        cv::Mat clone_frame = half_rgb_frame.clone();
        cv::rectangle(clone_frame, new_face_rect, Scalar(0, 255, 0), 3 );
        imwrite("face_dump.png", clone_frame);
#endif
        
        // Grabcut face.
        cv::Mat bgModel, fgModel;
        cv::Mat half_blob;
        cv::grabCut(half_rgb_frame, half_blob, new_face_rect, bgModel, fgModel, 10, cv::GC_INIT_WITH_RECT);
        
        // Get the pixels marked as likely foreground
        cv::compare(half_blob,cv::GC_PR_FGD,half_blob,cv::CMP_EQ);
        
        
        // Generate output image
        cv::Mat foreground(half_rgb_frame.size(),CV_8UC3,cv::Scalar(255,255,255));
        half_rgb_frame.copyTo(foreground, ~half_blob); // bg pixels not copied

        // Mophological operation to remove noise.
        cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                                            cv::Size( 5, 5 ) );

        cv::erode(half_blob, half_blob, element);
        cv::erode(half_blob, half_blob, element);
        cv::dilate(half_blob,half_blob, element);
        cv::dilate(half_blob,half_blob, element);
        
        // Resize blob image to get smooth curve.
        cv::Mat original_blob = cv::Mat(webtexture_image.size(), CV_8UC3);
        cv::resize(half_blob, original_blob, original_blob.size() );
        fgmask = original_blob.clone();
        
        
#ifdef CONTOUR_DEBUG
        imwrite("grabcut_blob.png", original_blob);
#endif
        return true;
    }
    
    return false;
}




inline cv::Mat calculate_affine_z_direction(cv::Mat affine_camera_matrix)
{
    using cv::Mat;
    // Take the cross product of row 0 with row 1 to get the direction perpendicular to the viewing plane (= the viewing direction).
    // Todo: We should check if we look/project into the right direction - the sign could be wrong?
    Mat affine_cam_z_rotation = affine_camera_matrix.row(0).colRange(0, 3).cross(affine_camera_matrix.row(1).colRange(0, 3));
    affine_cam_z_rotation /= cv::norm(affine_cam_z_rotation, cv::NORM_L2);
    
    // The 4x4 affine camera matrix
    Mat affine_cam_4x4 = Mat::zeros(4, 4, CV_32FC1);
    
    // Replace the third row with the camera-direction (z)
    Mat third_row_rotation_part = affine_cam_4x4.row(2).colRange(0, 3);
    affine_cam_z_rotation.copyTo(third_row_rotation_part); // Set first 3 components. 4th component stays 0.
    
    // Copy the first 2 rows from the input matrix
    Mat first_two_rows_of_4x4 = affine_cam_4x4.rowRange(0, 2);
    affine_camera_matrix.rowRange(0, 2).copyTo(first_two_rows_of_4x4);
    
    // The 4th row is (0, 0, 0, 1):
    affine_cam_4x4.at<float>(3, 3) = 1.0f;
    
    return affine_cam_4x4.clone();
};


inline cv::Mat to_mat(const glm::mat4x4& glm_matrix)
{
    // glm stores its matrices in col-major order in memory, OpenCV in row-major order.
    // Hence we transpose the glm matrix to flip the memory layout, and then point OpenCV
    // to that location.
    auto glm_matrix_t = glm::transpose(glm_matrix);
    cv::Mat opencv_mat(4, 4, CV_32FC1, &glm_matrix_t[0]);
    // we need to clone because the underlying data of the original goes out of scope
    return opencv_mat.clone();
};


inline cv::Mat get_3x4_affine_camera_matrix( float* modelview_mat, float* projection_mat, int width, int height)
{
    auto view_model = to_mat( glm::make_mat4(modelview_mat) );
    auto ortho_projection = to_mat( glm::make_mat4(projection_mat) );
    cv::Mat mvp = ortho_projection * view_model;
    
    glm::vec4 viewport(0, height, width, -height); // flips y, origin top-left, like in OpenCV
    // equivalent to what glm::project's viewport does, but we don't change z and w:
    cv::Mat viewport_mat = (cv::Mat_<float>(4, 4) <<
                            viewport[2] / 2.0f, 0.0f,               0.0f, viewport[2] / 2.0f + viewport[0],
                            0.0f,               viewport[3] / 2.0f, 0.0f, viewport[3] / 2.0f + viewport[1],
                            0.0f,               0.0f,               1.0f, 0.0f,
                            0.0f,               0.0f,               0.0f, 1.0f);
    
    cv::Mat full_projection_4x4 = viewport_mat * mvp;
    cv::Mat full_projection_3x4 = full_projection_4x4.rowRange(0, 3); // we take the first 3 rows, but then set the last one to [0 0 0 1]
    full_projection_3x4.at<float>(2, 0) = 0.0f;
    full_projection_3x4.at<float>(2, 1) = 0.0f;
    full_projection_3x4.at<float>(2, 2) = 0.0f;
    full_projection_3x4.at<float>(2, 3) = 1.0f;
    
    return full_projection_3x4;
};


inline cv::Mat get_affine_camera_matrix( float* modelview_mat, float* projection_mat, int width, int height)
{
    auto view_model = to_mat( glm::make_mat4(modelview_mat) );
    auto ortho_projection = to_mat( glm::make_mat4(projection_mat) );
    cv::Mat mvp = ortho_projection * view_model;
    
    glm::vec4 viewport(0, height, width, -height); // flips y, origin top-left, like in OpenCV
    // equivalent to what glm::project's viewport does, but we don't change z and w:
    cv::Mat viewport_mat = (cv::Mat_<float>(4, 4) <<
                            viewport[2] / 2.0f, 0.0f,               0.0f, viewport[2] / 2.0f + viewport[0],
                            0.0f,               viewport[3] / 2.0f, 0.0f, viewport[3] / 2.0f + viewport[1],
                            0.0f,               0.0f,               1.0f, 0.0f,
                            0.0f,               0.0f,               0.0f, 1.0f);
    
    cv::Mat full_projection_4x4 = viewport_mat * mvp;
    return full_projection_4x4;
};




void get_ellipse_mask(int cx,int cy,
                      int semi_major_axis,
                      int semi_minor_axis,
                      int width, int height,
                      unsigned char* mask)
{
    assert(mask);
    
    // Make ellipse mask.
    cv::Mat cv_mask_image(height, width, CV_8U, cv::Scalar(cv::GC_BGD) );
    
    cv::ellipse(cv_mask_image,
                cv::Point(cx, height-cy), //Flip Y-axis to match origin for Unity texture2D.
                cv::Size(semi_major_axis, semi_minor_axis),
                0,
                0,
                360,
                cv::Scalar(cv::GC_PR_FGD),
                -1);
    
    memcpy(mask,
            cv_mask_image.ptr(),
            width*height*sizeof(unsigned char)
            );
    return;
}



void _draw_head_mask(cv::Mat mask_image,
                     cv::Point face_center, cv::Size face_size,
                     cv::Point top_head_center, cv::Size top_head_size,
                     cv::Point neck_begin, cv::Point neck_end
                     )
{
    assert( mask_image.ptr() );
    
    int thickness = 10;
    
    
    // Face
    cv::ellipse(mask_image,
                face_center,
                face_size,
                0.0, 0.0, 360.0,
                cv::Scalar(cv::GC_PR_FGD),
                -1);

    // Top head
    cv::ellipse(mask_image,
                top_head_center,
                top_head_size,
                0.0, 0.0, 360.0,
                cv::Scalar(cv::GC_PR_FGD),
                -1);
    
    // Neck
    cv::rectangle(mask_image,
                  neck_begin,neck_end,
                  cv::Scalar(cv::GC_PR_FGD),
                  -1);
    
    
    // Face halo: To give background information, draw big halo.
    cv::ellipse(mask_image,
                cv::Point(face_center.x, mask_image.rows*1/3 ),
                cv::Size(face_size.width*3/2, (face_size.height+top_head_size.height)*3/4 ),
                0.0, 0.0, 360.0,
                cv::Scalar(cv::GC_PR_BGD),
                thickness);
}




void get_mask_from_face_landmark(float* facial_landmark,
                                   int length_of_facial_landmark,
                                   int width, int height,
                                   unsigned char* mask_for_unity,
                                   int tx, int ty,
                                   int face_width_comp, int face_height_comp,
                                   int face_tx, int face_ty,
                                   int head_width_comp, int head_height_comp,
                                   int head_tx, int head_ty,
                                   int neck_width_comp, int neck_height_comp )
{
    assert(facial_landmark);
    assert(mask_for_unity);
    assert(68 == length_of_facial_landmark);
    
    cv::Mat temp(height, width, CV_8UC1);
    temp = cv::Scalar(cv::GC_BGD);
    
    // From 0 to 16th points are chin points.
    std::vector<cv::Point> chin_points;
    chin_points.resize(17);
    for(int i=0; i<17; ++i)
    {
        chin_points[i].x = facial_landmark[2*i];
        chin_points[i].y = facial_landmark[2*i+1];
    }
    
    // Ellipse for face
    cv::RotatedRect ellipse_rect = cv::fitEllipse(chin_points);
    cv::Point face_center = ellipse_rect.center;
    cv::Size face_size(ellipse_rect.boundingRect().width/2 + face_width_comp/2,
                       ellipse_rect.boundingRect().height/2 + face_height_comp/2 );
    
    

    // Head part upper area of ellipse.
    cv::Point head_center(ellipse_rect.center.x,
                          ellipse_rect.center.y
                          - ellipse_rect.boundingRect().height/10);
    int head_major_axis = ellipse_rect.boundingRect().width/2*1.4f+head_width_comp/2;
    int head_minor_axis = ellipse_rect.boundingRect().width/2*1.2f+head_width_comp/2;
    cv::Size head_size(head_major_axis, head_minor_axis);
    
    
    // Neck
    cv::Point neck_start(chin_points[3].x-neck_width_comp/2, face_center.y - neck_height_comp/2);
    int neck_height = (ellipse_rect.boundingRect().height + head_minor_axis)/3;
    cv::Point neck_end(chin_points[13].x+neck_width_comp/2, chin_points[13].y + neck_height+neck_height_comp/2);
    
    
    // Add translation in 2D.
    face_center += cv::Point(tx+face_tx,   ty+face_ty);
    head_center += cv::Point(tx/2+head_tx, ty+head_ty);
    neck_start  += cv::Point(0, ty);
    neck_end    += cv::Point(0, ty);
    
    // Readjust head center and face center.
    int temp2 = head_center.x + head_size.width;
    if ( temp2 > width )
        head_center.x -= (width - temp2);
    
    int temp3 = head_center.x - head_size.width;
    if( temp3 < 0)
        head_center.x -= temp3*2;
    
    int temp4 = face_center.x - face_size.width;
    if( temp4 < 0)
        face_center.x -= temp4*2;
    
    int temp5 = head_center.y - head_size.height;
    if (temp5 < 0)
        head_center.y = head_size.height-10;
    

    // Draw all parts.
    _draw_head_mask(temp, face_center, face_size, head_center, head_size, neck_start, neck_end);
    
    // Flip for unity
    cv::flip(temp, temp, 0);
    
    
    //Copy memory
    memcpy(mask_for_unity, temp.ptr(), width*height);
    temp.release();
}


void get_side_mask_from_face_landmark(float* face_landmark,
                                      int length_of_face_landmark,
                                      int width, int height,
                                      bool is_left_side,
                                      unsigned char* mask_for_unity,
                                      int face_width_comp, int face_height_comp,
                                      int face_tx, int face_ty,
                                      int head_width_comp, int head_height_comp,
                                      int head_tx, int head_ty,
                                      int neck_width_comp, int neck_height_comp )
{
    int tx = 0;
    
    // Left side face = the nose is left side.
    if (is_left_side)
    {
        // Get distance from right eye outer-corner (1) (37) to left eye pupil top right(44)
        tx = int( face_landmark[2*36] - face_landmark[2*43] );

    }else
    {
        // Get distance from left eye pupil top right(44) to right eye outer-corner (1) (37)
        tx = int( face_landmark[2*43] - face_landmark[2*36] );
        
    }

    get_mask_from_face_landmark(face_landmark,length_of_face_landmark,
                                width, height,
                                mask_for_unity,
                                tx, 0,
                                face_width_comp, face_height_comp,
                                face_tx,         face_ty,
                                head_width_comp, head_height_comp,
                                head_tx,         head_ty,
                                neck_width_comp, neck_height_comp );
}





void get_mask_for_profile_face(int rect_x, int rect_y,
                               int rect_width, int rect_height,
                               unsigned char* mask_for_unity,
                               int width, int height)
{
    
    assert(mask_for_unity);
    cv::Mat mask_image(height, width, CV_8UC1, mask_for_unity);
    mask_image = cv::Scalar(cv::GC_BGD);
    
    int head_rect[4]={rect_x, rect_y, rect_width, rect_height};
    
    // Front head
    cv::Point front_head_center (int(head_rect[0] + head_rect[2] / 2),
                                   int(head_rect[1] + head_rect[3] / 2));
    cv::Size front_head_size(int(float(head_rect[2]) / 2.0f * 0.7f) ,
                             int(head_rect[3] / 2));
    
    // Back head
    cv::Point back_head_center(int(front_head_center.x + head_rect[3] / 2.0 * 0.8f),
                               head_rect[1] );
    int back_head_radius = int( float(head_rect[2])*0.7f);
    
    // Compensate back head dimension.
    if ( (back_head_center.y - back_head_radius) < 0 )
    {
        int displacement = back_head_radius - back_head_center.y;
        back_head_center = cv::Point(
                                     int( float(back_head_center.x) - displacement/2),
                                     int( float(back_head_center.y) + displacement)
                                     );
    }
    
    // Neck
    cv::Point neck_begin( front_head_center.x, int( float(front_head_center.y)*0.7f) );
    cv::Point neck_end(
                int(back_head_center.x + float(back_head_radius) * 0.7f),
                int(back_head_center.y + back_head_radius + front_head_size.height )
            );
    
    // Draw all parts.
    _draw_head_mask( mask_image,
                    front_head_center, front_head_size,
                    back_head_center, cv::Size(back_head_radius, back_head_radius),
                    neck_begin, neck_end);
    
    // Flip horozontally for Unity.
    cv::flip(mask_image, mask_image, 0);
}


bool get_side_face_mask( unsigned char* image_for_unity,
                         int width, int height,
                         unsigned char* mask_for_unity)
{
    
    int rect_x, rect_y, rect_width, rect_height;
    bool res = get_profile_face_rect(image_for_unity,
                                     width,
                                     height,
                                     &rect_x, &rect_y,
                                     &rect_width, &rect_height);
    
    if(res)
    {
        // It has to be left face.
        cv::Mat left_mask_for_unity(height, width, CV_8UC1, mask_for_unity);
        get_mask_for_profile_face(rect_x, rect_y,
                                  rect_width, rect_height,
                                  left_mask_for_unity.ptr(),
                                  width,
                                  height);
        return true;
    }else{
        
        // It can be right face. So we'll flip left and right and do that.
        cv::Mat right_face_image
            = cv::Mat(height, width, CV_8UC4, image_for_unity, cv::Mat::AUTO_STEP);
        cv::flip(right_face_image, right_face_image, 1); // Flip left and right.
        
        res = get_profile_face_rect(right_face_image.ptr(),
                              width,
                              height,
                              &rect_x, &rect_y,
                              &rect_width, &rect_height);

        
        // If image has face then draw initial mask.
        if (res)
        {
            cv::Mat right_mask_for_unity(height, width, CV_8UC1, mask_for_unity);
            get_mask_for_profile_face(rect_x, rect_y,
                                      rect_width, rect_height,
                                      right_mask_for_unity.ptr(),
                                      width,
                                      height);
            cv::flip(right_mask_for_unity, right_mask_for_unity,1);
            return true;            
        }

        return false;
    }
    
    return false;
}


void get_foreground_mask_by_grab_cut(unsigned char* bgr_image, int width, int height, int iter, unsigned char* mask)
{
    // Convert bgr_image to cv::Mat.
    cv::Mat frame(height, width, CV_8UC3, bgr_image);
    
    // Run GrapCut.
    cv::Mat bg_model, fg_model;
    cv::Mat mask_image(frame.size(), CV_8U, mask);
    cv::Rect rect( 0, 0, frame.cols, frame.rows);
    cv::grabCut(frame, mask_image, rect, bg_model, fg_model, iter, cv::GC_INIT_WITH_MASK);
    
    // Get the pixels marked as likely foreground. It converts GC_PR_FGD to 255.
    cv::compare(mask_image,cv::GC_PR_FGD,mask_image,cv::CMP_EQ);
    
    // Mophological operation to remove noise.
    cv::Mat big_element = cv::getStructuringElement( cv::MORPH_RECT,
                                                    cv::Size( 7, 7 ) );
    
    cv::Mat small_element = cv::getStructuringElement( cv::MORPH_RECT,
                                                      cv::Size( 7, 7 ) );
    
    cv::dilate(mask_image,mask_image, big_element);
    cv::dilate(mask_image,mask_image, big_element);
    
    cv::erode(mask_image,mask_image, small_element);
    cv::erode(mask_image,mask_image, small_element);
    
    // Convert 255 in mask to GC_PR_FGD.
    mask_image = mask_image.mul( (double)1.0/255.0);
    mask_image = mask_image.mul( (double)cv::GC_PR_FGD);
    
    // Copy mask image to buffer.
    memcpy(mask,
            mask_image.ptr(),
            mask_image.size().area()*sizeof(unsigned char)
            );
    
}


void refine_mask_by_grabcut(unsigned char* webcam_texture,
                            int width, int height,
                            int iter,
                            unsigned char* mask,
                            bool use_hand_painted_mask)
{
    assert(webcam_texture);
    assert(mask);
    
    const int GC_USER_FGD=4;
    const int GC_USER_BGD=5;
    
    
    // Flip image.
    cv::Mat webtexture_image(height, width, CV_8UC4, webcam_texture);
    cv::Mat fliped_image;
    cv::flip(webtexture_image,  fliped_image, 0);
    cv::cvtColor(fliped_image, fliped_image, cv::COLOR_BGRA2BGR);
    
    
    //Flip mask image from unity texture2d to opencv mat.
    cv::Mat mask_image(fliped_image.size(), CV_8UC1, mask);
    cv::flip(mask_image, mask_image, 0);
    
    // Extract user painted background and foreground.
    cv::Mat mask_backup = mask_image.clone();
    cv::Mat mask_grabcut = cv::Mat::zeros( mask_image.size(), CV_8UC1 );
    mask_backup.copyTo(mask_grabcut);

    if( use_hand_painted_mask )
    {
        for( int i= 0; i<mask_image.size().width; ++i)
        {
            for (int j=0; j<mask_image.size().height; ++j)
            {
                if( GC_USER_FGD == mask_grabcut.at<uchar>(j, i) )
                    mask_grabcut.at<uchar>(j, i) = cv::GC_FGD;
                else if ( GC_USER_BGD == mask_grabcut.at<uchar>(j, i) )
                    mask_grabcut.at<uchar>(j, i) = cv::GC_BGD;
            }
        }
        
    }
    
    // Do grabcut.
    get_foreground_mask_by_grab_cut(fliped_image.ptr(),
                                    fliped_image.cols,
                                    fliped_image.rows,
                                    iter,
                                    mask_grabcut.ptr() );
    
    // Compare the src mask and result and set pixel as foreground if grabcut missed.
    if( use_hand_painted_mask )
    {
        for( int i= 0; i<mask_image.size().width; ++i)
        {
            for (int j=0; j<mask_image.size().height; ++j)
            {
                bool is_BGD = ( cv::GC_BGD == mask_grabcut.at<uchar>(j, i) );
                bool is_PR_BGD = ( cv::GC_PR_BGD == mask_grabcut.at<uchar>(j, i) );
                bool is_USER_FGD = ( GC_USER_FGD == mask_backup.at<uchar>(j, i) );
                
                if ( (is_BGD||is_PR_BGD) && is_USER_FGD )
                {
                    mask_grabcut.at<uchar>(j, i) = cv::GC_PR_FGD;
                }
                
            }
        }
    }

    mask_grabcut.copyTo(mask_image);
    
    //Flip mask image for unity.
    cv::flip(mask_image, mask_image, 0);
    
}


void dump_3d_data( std::vector<std::vector<cv::Vec3f>>& pt, const char* filepath)
{
    // DUMP OBJ file as test.
    FILE*  pFile = fopen( filepath,"w");
    for ( int i=0; i < pt.size(); ++i)
    {
        for ( int j=0; j<pt[i].size(); ++j )
        {
            fprintf(pFile, "v %f %f %f\n",
                    pt[i][j][0],
                    pt[i][j][1],
                    pt[i][j][2]
                    );
        }
    }
    fclose(pFile);
}

bool build_head_volume(unsigned char* front_mask,
                       unsigned char* right_mask,
                       int width, int height,
                       float* modelview_mat,
                       float* projective_mat,
                       const char* file_path,
                       int lod_of_depth,
                       int lod_of_top_circle)
{
    // Build volume.
    cv::Mat front_mask_mat(height, width, CV_8UC1, front_mask);
    cv::Mat right_mask_mat(height, width, CV_8UC1, right_mask);
    
    //Flip mask image from unity texture2d to opencv mat.
    cv::flip(front_mask_mat, front_mask_mat, 0);
    cv::flip(right_mask_mat, right_mask_mat, 0);
    

    // Get the pixels marked as likely foreground. It converts GC_PR_FGD to 255.
    cv::threshold(front_mask_mat, front_mask_mat, 1.0, 255.0, cv::THRESH_BINARY);
    cv::threshold(right_mask_mat, right_mask_mat, 1.0, 255.0, cv::THRESH_BINARY);

    //DEBUG
//    imwrite("front_mask.png", front_mask_mat);
//    imwrite("right_mask.png", right_mask_mat);
    
    
    // Align right_mask on front_mask.
    cv::Mat new_right_mask;
    cv::Point translate;
    allign_right_mask_on_front_mask(front_mask_mat, right_mask_mat, new_right_mask,translate);

    
    // Build 3D model from two blobs in image coordinate.
    std::vector< std::vector<cv::Vec3f> > points_data;
    
    bool ret = false;
    
    ret = bulld_3d_points_from_two_blob(front_mask_mat,
                                  new_right_mask,
                                  points_data,
                                  lod_of_depth,
                                  lod_of_top_circle);
    
    if( ret )
    {
        //dump_3d_data(points_data, "3d_dump0.obj");
        
        // Transform object in object coordinate.
        
        if( NULL != modelview_mat && NULL != projective_mat )
        {
            transform_vertices_in_local_coord(points_data,
                                              modelview_mat,
                                              projective_mat,
                                              width,
                                              height);
        }
        
        //dump_3d_data(points_data, "3d_dump1.obj");
        
        // Build mesh data structure and store it as OBJ file.
        MyMesh m;
        make_3D_mesh_from_points_cloud(points_data, m, file_path);
        return true;
    }else
    {
        std::cerr << "Fail to build volume." << std::endl;
        return false;
    }
    
}


cv::RotatedRect find_rotated_rectangle(cv::Mat binary_mask)
{
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(binary_mask.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);
    
    // Find the longest contours in the list.
    int max_size =0, contour_index = 0;
    for( int i=0; i<contours.size(); ++i)
    {
        if ( max_size<contours[i].size() )
        {
            max_size = contours[i].size();
            contour_index = i;
        }
    }
    
    cv::RotatedRect rect = cv::fitEllipse(contours[contour_index]);
    
    
    if (rect.angle > 90.0)
    {
        rect.angle = 180.0-rect.angle;
    }
    
    return rect;
}


cv::Rect find_roi_rectangle(cv::Mat binary_mask)
{
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(binary_mask.clone(), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);
    
    // Find the longest contours in the list.
    int max_size =0, contour_index = 0;
    for( int i=0; i<contours.size(); ++i)
    {
        if ( max_size<contours[i].size() )
        {
            max_size = contours[i].size();
            contour_index = i;
        }
    }
    
    cv::Rect rect = cv::boundingRect(contours[contour_index]);
    return rect;
}


cv::Mat draw_rotated_rect( cv::Mat img, cv::RotatedRect rotated_rect)
{
    cv::Mat temp1 = img.clone();
    cv::cvtColor(temp1, temp1, cv::COLOR_GRAY2BGR);
    cv::Point2f points[4];
    rotated_rect.points(points);
    for(int i =0; i<4; i++){
        line(temp1, points[i], points[(i+1)%4], Scalar(0,0,255));
    }
    cv::circle(temp1,rotated_rect.center, 5, Scalar(0, 255, 0) );
    cv:rectangle(temp1, rotated_rect.boundingRect(), Scalar(255, 0, 0) );
    return temp1.clone();
    
}

void allign_right_mask_on_front_mask( cv::Mat front_mask, cv::Mat right_mask, cv::Mat& new_right_mask, cv::Point& translate)
{
    cv::Moments front_image_moments = cv::moments(front_mask);
    cv::Moments right_image_moments = cv::moments(right_mask);
    cv::Point front_centroid = cv::Point (front_image_moments.m10/front_image_moments.m00,
                                          front_image_moments.m01/front_image_moments.m00);
    cv::Point right_centroid = cv::Point (right_image_moments.m10/right_image_moments.m00,
                                          right_image_moments.m01/right_image_moments.m00);
    
    translate = cv::Point( front_centroid.x-right_centroid.x,
                           front_centroid.y-right_centroid.y);
    
    // Get rotated rectangle.
    cv::RotatedRect front_rotated_rect = find_rotated_rectangle(front_mask);
    cv::RotatedRect right_rotated_rect = find_rotated_rectangle(right_mask);
    
    
// DEBUG
//std::cout<<"Front mask's angle: "<< front_rotated_rect.angle << std::endl;
//std::cout<<"Right mask's angle: "<< right_rotated_rect.angle << std::endl;
//cv::imwrite("front_dump.png", draw_rotated_rect(front_mask, front_rotated_rect) );
//cv::imwrite("right_dump.png", draw_rotated_rect(right_mask, right_rotated_rect) );
    
    
    /// Apply the Affine Transform just found to the src image
    float area_ratio = 1.0f;
    cv::Mat affine_transform
        = getRotationMatrix2D(right_rotated_rect.center,
                              right_rotated_rect.angle,
                              area_ratio );
    affine_transform.at<double>(0,2) += (float)translate.x;
    affine_transform.at<double>(1,2) += (float)translate.y;
    cv::Mat temp = cv::Mat::zeros(front_mask.size(),
                                  front_mask.type() );
    warpAffine( right_mask, temp, affine_transform, temp.size() );

    // DEBUG
//    std::cout << affine_transform << std::endl;

    // Threshold again to remove clutter by warphing.
    cv::threshold(temp, temp, 1.0, 255.0, cv::THRESH_BINARY);
    
    // DEBUG
//    cv::imwrite("new_right.png", temp );

    
    // Copy the result.
    new_right_mask = temp.clone();
}

bool get_silhouette_in_image(cv::Mat& webtexture_image, cv::Rect rect, cv::Mat& fgmask)
{
    // Convert image in RGB format and left-top origin.
    cv::Mat rgb_frame = cv::Mat(webtexture_image.cols, webtexture_image.rows, CV_8UC3);
    cv::cvtColor(webtexture_image, rgb_frame, cv::COLOR_BGRA2RGB);
    cv::flip(rgb_frame, rgb_frame, 0);
    
    
    // Grabcut face.
    cv::Mat bgModel, fgModel;
    cv::Mat blob;
    cv::grabCut(rgb_frame, blob, rect, bgModel, fgModel, 10, cv::GC_INIT_WITH_RECT);
    
    // Get the pixels marked as likely foreground
    cv::compare(blob,cv::GC_PR_FGD,blob,cv::CMP_EQ);
    
    // Generate output image
    cv::Mat foreground(rgb_frame.size(),CV_8UC3,cv::Scalar(255,255,255));
    rgb_frame.copyTo(foreground, ~blob); // bg pixels not copied
    
    // Mophological operation to remove noise.
    cv::Mat element = cv::getStructuringElement( cv::MORPH_RECT,
                                                cv::Size( 5, 5 ) );
    cv::erode(blob, blob, element);
    cv::erode(blob, blob, element);
    cv::dilate(blob,blob, element);
    cv::dilate(blob,blob, element);
    
    fgmask = blob.clone();
    return true;
}


void get_unit_circle_points_on_xz_plan( int step, std::vector<cv::Vec3f>& points)
{
    const float PI=3.14159265;
    
    points.clear();
    for ( int i=0; i<step; ++i)
    {
        float angle_rad = ( i*360.0f/(float)step )*PI/180.0f;
        float x = cos( angle_rad );
        float y = sin( angle_rad );
        
        points.push_back( cv::Vec3f(x, 0.0f ,y)  );
         
    }
    
}

void warp_unit_circle_points_on_xz_plan(
                            std::vector<cv::Vec3f>& points,
                            float length_of_axis_x, float length_of_axis_z,
                            float tx, float tz,
                            float depth_of_layer_on_y_axis,
                            std::vector<cv::Vec3f>& output_points)
{
    output_points.clear();
    
    float sx = length_of_axis_x;
    float sz = length_of_axis_z;
    
    for(int i=0; i<points.size(); ++i){
        
        output_points.push_back(
                                cv::Vec3f(
                                      sx * points[i][0] + tx,
                                      depth_of_layer_on_y_axis,
                                      sz * points[i][2] + tz
                                    )
                                );
    }
}

bool get_begin_and_end_index_in_row(cv::Mat row_binary_image, int& begin, int& end)
{
    std::vector<int> line_begin, line_end;
    
    uchar val[3];
    for( int i=1; i<=row_binary_image.cols; ++i)
    {
        if( 255 == row_binary_image.at<uchar>(0,i) )
        {
            val[0] = row_binary_image.at<uchar>(0,i-1);
            val[1] = row_binary_image.at<uchar>(0,i);
            val[2] = row_binary_image.at<uchar>(0,i+1);
            
            if( 0 == val[0] && 255 == val[1] && 255 == val[2] )
                line_begin.push_back(i);
            
            if( 255 == val[0] && 255 == val[1] && 0 == val[2] )
            {
                line_end.push_back(i);
            }
        }
    }
    
    //Remain longest line's begin and end.
    int max_length = 0, ind_length = 0;
    for(int i=0 ; i<line_begin.size(); ++i )
    {
        int length = line_end[i] - line_begin[i];
       
        if( max_length < length )
        {
            max_length = length;
            ind_length = i;
        }
    }
    
    if (line_begin.size() != line_end.size() ){
        std::cerr<< "Fail to find begin and end point in this row." << std::endl;
        return false;
    }
    
    begin = line_begin[ind_length];
    end = line_end[ind_length];
    
    
    if ( 0 == begin || 0 == end)
    {
        std::cerr<< "Fail to find begin and end point in this row." << std::endl;
        return false;
    }

    return true;
}




bool bulld_3d_points_from_two_blob(cv::Mat& front_mask,
                                   cv::Mat& right_mask,
                                   std::vector< std::vector<cv::Vec3f> >& points_data,
                                   int lod_of_depth,
                                   int lod_of_top_circle)
{
    assert( front_mask.size().width );
    assert( front_mask.size().height );
    
    assert( right_mask.size().width );
    assert( right_mask.size().height );
    
    assert(lod_of_depth > 0);
    assert(lod_of_top_circle > 0);

    // Clear points data. 
    points_data.clear();
    
    // Get center of each image.
    cv::Moments front_mask_moment = cv::moments(front_mask, true);
    cv::Point center_of_front_mask(
                                   front_mask_moment.m10 / front_mask_moment.m00,
                                   front_mask_moment.m01 / front_mask_moment.m00
                                   );

    cv::Moments right_mask_moment = cv::moments(right_mask, true);
    cv::Point center_of_left_mask (
                                   right_mask_moment.m10 / right_mask_moment.m00,
                                   right_mask_moment.m01 / right_mask_moment.m00
                                   );
    
    // Build volume by accumulating warped unit circle.
    std::vector<cv::Vec3f> points_on_unit_circle;
    get_unit_circle_points_on_xz_plan(lod_of_top_circle, points_on_unit_circle);
    
    
    int depth_of_volume = front_mask.rows;
    int depth_stride = depth_of_volume/lod_of_depth;
    
    assert(depth_stride);
    
    for ( int d = 0; d< depth_of_volume; d=d+depth_stride )
    {
        float length_of_axis_x = (float)(cv::sum( front_mask.row(d) )[0]/255); // Crash?
        float length_of_axis_z = (float)(cv::sum( right_mask.row(d) )[0]/255);
        
        
        if( length_of_axis_x!=0.0f && length_of_axis_z != 0.0f )
        {

            // Initially put one point to make the mesh for top area.
            if( points_data.empty() )
            {
                std::vector<cv::Vec3f> points_on_layer;
                points_on_layer.push_back(cv::Vec3f( 0.0f,d,0.0f) );
                points_data.push_back( points_on_layer );
            }else
            {
                // Make the layer by warping the unit circle.
                std::vector<cv::Vec3f> points_on_layer;
                
                int tx_begin, tx_end;
                int length_of_axis_x, cx, cy, tx;
                if( get_begin_and_end_index_in_row(front_mask.row(d), tx_begin, tx_end) )
                {
                    length_of_axis_x = (tx_end - tx_begin)/2;
                    cx = length_of_axis_x + tx_begin;
                    tx = center_of_front_mask.x - cx;
                }else{
                    // Fail to build volume. !!
                    std::cerr << "Fail to build volume with current mask images. Mask image have to be convex and no hole." << std::endl;
                    return false;
                }
                
                int tz_begin, tz_end;
                int length_of_axis_z, cz, tz;
                
                if( get_begin_and_end_index_in_row(right_mask.row(d), tz_begin, tz_end) )
                {
                    length_of_axis_z = (tz_end - tz_begin)/2;
                    cz = length_of_axis_z + tz_begin;
                    tz = center_of_left_mask.x - cz;
                    
                    
                }else{
                    // Fail to build volume. !!
                    std::cerr << "Fail to build volume with current mask images. Mask image have to be convex and no hole." << std::endl;
                    return false;
                }
                
                // Warp unit circle to reshape the contour in layer.
                warp_unit_circle_points_on_xz_plan(
                                                   points_on_unit_circle,
                                                   length_of_axis_x,
                                                   length_of_axis_z,
                                                   tx, tz,
                                                   (float)d,
                                                   points_on_layer
                                                   );

                // Stack that layer.
                points_data.push_back( points_on_layer );
                
            }
            
        }
    }
    
    
    return true;
}

void transform_vertices_in_local_coord(std::vector< std::vector<cv::Vec3f> >& points,
                                     float* modelview_mat,
                                     float* projective_mat,
                                     int width, int height,
                                     float* new_modelview_mat,
                                     float* new_projective_mat)
{
    glm::mat4 modelview = glm::make_mat4(modelview_mat);
    glm::mat4 projective = glm::make_mat4(projective_mat);
    glm::vec4 viewport(0, height, width, -height);
    
//    std::cout << glm::to_string(modelview)  << std::endl;
//    std::cout << glm::to_string(projective) << std::endl;

    // Rotate 180 degree about Y-axis to fit on 4D face's coordinates.
    glm::mat4 new_modelview = glm::mat4(1.0);
    
    // Translate volume slightly.
    glm::vec3 compenstate(0.0f, 0.0f, 0.0f );
    
    new_modelview = glm::rotate(new_modelview,
                                glm::radians(180.0f),
                                glm::vec3(0.0f, 1.0f, 0.0f)
                                );
    new_modelview = glm::translate(new_modelview,
                                   glm::vec3( 0.0f,
                                              modelview_mat[13],
                                              modelview_mat[14] )
                                   );
    
    // Measure original model's 3D ROI
    glm::vec3 org_min_box(0.0f, 0.0f, 0.0f), org_max_box(0.0f, 0.0f, 0.0f);
    for ( int i=0; i < points.size(); ++i)
    {
        for ( int j=0; j<points[i].size(); ++j )
        {
            for( int k=0; k<3; ++k){
                if( points[i][j][k] < org_min_box[k] )
                    org_min_box[k] = points[i][j][k];

                
                if( points[i][j][k] > org_max_box[k] )
                    org_max_box[k] = points[i][j][k];
            }
        }
    }
    float org_width_volume  = fabs( org_max_box[0] - org_min_box[0] );
    float org_height_volume = fabs( org_max_box[1] - org_min_box[1] );
    float org_depth_volume  = fabs( org_max_box[2] - org_min_box[2] );

    
    
    
    // Transform by unprojecting points in image coordinate.
    glm::vec3 min_box(0.0f, 0.0f, 0.0f), max_box(0.0f, 0.0f, 0.0f);
    for ( int i=0; i < points.size(); ++i)
    {
        for ( int j=0; j<points[i].size(); ++j )
        {
            
            glm::vec3 pos
                = glm::unProject({points[i][j][0], points[i][j][1], points[i][j][2]},
                                 new_modelview, projective, viewport);
            
            points[i][j][0] = pos[0];
            points[i][j][1] = pos[1];
            points[i][j][2] = pos[2];
            
            
            // Calculate min/max 3D ROI.
            for( int k=0; k<3; ++k){
                if( pos[k] < min_box[k] )
                    min_box[k] = pos[k];

                if( pos[k] > max_box[k] )
                    max_box[k] = pos[k];
            }
            
            
        }
    }
    
    // Rescale Z-depth to X-Width for human proportion. And translate in Z direction to fit on 4D face's coordinates.
    float width_volume  = fabs( max_box[0] - min_box[0] );
    float height_volume = fabs( max_box[1] - min_box[1] );
    float depth_volume  = fabs( max_box[2] - min_box[2] );
    
    //float re_z_scale = 1.0/depth_volume * (width_volume*1.2f); // See https://pacificgraphicdesign.wordpress.com/courses/computer-graphics-print-media/assignments-arts-091/drawing-the-human-head/head-anatomy-proportions-1/
    
    float re_z_scale = 1.0/depth_volume * (width_volume*org_depth_volume/org_width_volume);
    
    
    min_box = glm::vec3(0.0f, 0.0f, 0.0f);
    max_box = glm::vec3(0.0f, 0.0f, 0.0f);
 
    for ( int i=0; i < points.size(); ++i)
    {
        for ( int j=0; j<points[i].size(); ++j )
        {
            points[i][j][2] = re_z_scale*points[i][j][2];
            
            // Calculate min/max 3D ROI.
            for( int k=0; k<3; ++k){
                if( points[i][j][k] < min_box[k] )
                    min_box[k] = points[i][j][k];
                
                if( points[i][j][k] > max_box[k] )
                    max_box[k] = points[i][j][k];
            }
        }
        
    }
    
    // Translate volume in Z-axis.
    float new_depth_translate = min_box[2];
    for ( int i=0; i < points.size(); ++i)
    {
        for ( int j=0; j<points[i].size(); ++j )
        {
            points[i][j][2] = points[i][j][2] + new_depth_translate;
        }
        
    }
    
    
    // Recompose modelview and projective matrix.
    new_modelview[2][3] -= new_depth_translate;
    projective[2][2] = re_z_scale * projective[2][2];

    //  std::cout << glm::to_string(new_modelview)  << std::endl;
    //  std::cout << glm::to_string(projective) << std::endl;
    
    if( new_modelview_mat && new_projective_mat)
    {
        
        for(int i=0; i<16; ++i)
        {
            new_modelview_mat[i] = glm::value_ptr(new_modelview)[i];
            new_projective_mat[i] = glm::value_ptr(projective) [i];
        }
    }
    
    
    
}



void make_3D_mesh_from_points_cloud(std::vector< std::vector<cv::Vec3f> >& points,
                                    MyMesh &m,
                                   const char* model_path_file)
{
    int number_of_vertiecs = (points.size()-1)*points[1].size()+1;
    int stride = points[1].size();
    
    // Prepare the VCG data structure.
    MyMesh::VertexIterator vi = vcg::tri::Allocator<MyMesh>::AddVertices(m,number_of_vertiecs);
    
    // Add verticies.
    for( int i=0; i<points.size(); ++i)
    {
        for( int j=0; j<points[i].size(); ++j)
        {
            vi->P() = MyMesh::CoordType(points[i][j][0],
                                        points[i][j][1],
                                        points[i][j][2] );
            ++vi;
        }
    }
    
    
    // Build 1st top mesh faces.
    //It's radial circle. Center is 1st vertices and stride'th verticies are surrounding this vertex.
    //
    //  Example: Stiride = 6,
    //
    //            [V2]
    //  [V1]                 [V3]
    //
    //          [1st V]
    //  [V6]                 [V4]
    //
    //           [V5]
    //
    MyMesh::VertexPointer ivp[stride+1];
    vi = m.vert.begin();
    ivp[0] = &*vi;
    vi++;
    
    for( int i=1; i<=stride; ++i)
    {
        ivp[i] = &*vi;  ++vi;
    }
    
    for( int i=1; i<=stride; ++i)
    {
        vcg::tri::Allocator<MyMesh>::AddFace(m,
                                             ivp[0],
                                             ivp[i],
                                             ivp[i%stride+1]);
    }
    
    // Build other faces.
    vi = m.vert.begin();vi++;
    MyMesh::VertexIterator vetex_begin = vi;
    for ( ; vi!= (m.vert.end()-stride); ++vi)
    {
        
        // Upper face:
        // [vi]-------[vi+1]
        //  |           /
        //  |          /
        // [vi+stride]/
        
        int new_idx = (vi-vetex_begin)%stride;
        
        ivp[0] = &*vi;
        ivp[1] = &*(vi+stride);
        
        // Need to make last face with 1st vertex.
        if (new_idx == stride-1)
            ivp[2] = &*(vi-(stride-1));
        else
            ivp[2] = &*(vi+1);
        
        
        vcg::tri::Allocator<MyMesh>::AddFace(m,
                                             ivp[0],
                                             ivp[1],
                                             ivp[2]);
        
        
        // Lower face:
        //                   [vi+1]
        //                 /      |
        //                /       |
        //               /        |
        //              /         |
        //             /          |
        //            /           |
        //           /            |
        // [vi+stride]-------[vi+stride+1]
        new_idx = (vi-vetex_begin)%stride;

        // Need to make last face with 1st vertex.
        if (new_idx == (stride-1))
        {
            ivp[0] = &*(vi-(stride-1));
            ivp[2] = &*(vi-(stride-1)+stride);
        }else
        {
            ivp[0] = &*(vi+1);
            ivp[2] = &*(vi+stride+1);
        }
        ivp[1] = &*(vi+stride);
        
        
        vcg::tri::Allocator<MyMesh>::AddFace(m,
                                             ivp[0],
                                             ivp[1],
                                             ivp[2]);
        
    }
    
    vcg::tri::io::ExporterOBJ<MyMesh>::Save(m,
                                            model_path_file,
                                            vcg::tri::io::Mask::IOM_VERTCOORD);
    
    return;
    
}


void get_roi_of_mask_image_for_unity(unsigned char* mask_for_unity, int width, int height,
                           int* rect_x, int* rect_y, int* rect_width, int* rect_height)
{
    cv::Mat mask_image = cv::Mat(height, width, CV_8UC1, mask_for_unity, cv::Mat::AUTO_STEP);
    cv::Mat temp = mask_image.clone();
    cv::flip(temp, temp, 0);
    
    cv::Rect rect = find_roi_rectangle(temp);
    
    *rect_x = rect.x;
    *rect_y = rect.y; //Inverse for unity's origin. (Left, bottom)
    *rect_width = rect.width;
    *rect_height = rect.height;
    
    temp.release();
}










