//
//  face_scanner.h
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 3. 3..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//

#ifndef face_scanner_h
#define face_scanner_h



#ifdef __cplusplus
extern "C" {
#endif

    /**
     * @brief Initialize face scanner module.
     *
     * You have to call this function for every
     *
     * @param[in] model_file model file
     * @param[in] mappings_file mapping file
     * @param[in] contour_file countour data file
     * @param[in] landmarkdetector_file land mark of face detector data file.
     * @param[in] facedetector_file data file for face detector.
     * @param[in] profile_facedetector_file data file for profile face detector.
     * @param[in] blendshapes_file data file
     * @param[in] edgetopology_file data file
     * @param[in] normals_file normal data of current general mesh data file.
     * @return true = succeed to initialize, false = There are some error.
     */

    int init_face_scanner(const char* model_file, const char* mappings_file,
                          const char* contour_file,const char* landmarkdetector_file,
                          const char* facedetector_file, const char* profile_facedetector_file,
                          const char* blendshapes_file, const char* edgetopology_file,
                          const char* normals_file);
    
    
    /**
     * @brief Do 4DFaces process and reconstruct 3d data.
     *
     * You can scan several face images and reconstruct face data.
     *
     * @param[in] current_image image data. It must be BGRA format bitmap data. 
     * @param[in] width  width of image data.
     * @param[in] height height of image data.
     * @return true = Found the face in current image. So you can call capture_mesh_and_texture() to get the result. false = you didn't call init_face_scanner() before calling this function or there are some error
     */
    bool detect_and_track_face(unsigned char* current_image, int width, int height);


    /**
     * @brief Find face landmark in crrent image.
     *
     * Find face landmark from current image
     *
     * @param[in] current_image image data. It must be BGRA format bitmap data.
     * @param[in] width  width of image data.
     * @param[in] height height of image data.
     * @return true = Found the face lanrmark in current image. false = you didn't call init_face_scanner() before calling this function or there are some error
     */
    bool find_face_landmark(unsigned char* current_image, int width, int height);
    
    /**
     * @brief Find face landmark in crrent image quickly.
     *
     * Find face landmark from current image quicly.

     * @param[in] prev_image_for_unity previous image data. It must be BGRA format bitmap data.
     * @param[in] cur_image_for_unity image data. It must be BGRA format bitmap data.
     * @param[in] width  width of image data.
     * @param[in] height height of image data.
     * @param[in] window_size  window size to search the nexe landmark position. Bigger this value, faster and better to follow movement of face landmark. Smaller this value, slower and more correct to follow the face landmark.
     * @return true = Found the face landmark in current image. false = you didn't call init_face_scanner() before calling this function or there are some error
     */
    bool quick_find_face_landmark(unsigned char* prev_image_for_unity,
                                  unsigned char* cur_image_for_unity,
                                  int width, int height, int window_size = 10);

    
    bool find_face_rect(unsigned char* cur_image_for_unity,
                        int width, int height,
                        int* rect_x, int* rect_y, int* rect_width, int* rect_height);

    
    /**
     * @brief Reconstruct mesh structure and texture from current image.
     *
     * Write current 3d model and facial texture into the storage.
     *
     * @param[in] path file path to store data.
     * @param[in] filename  Model file name is 'filename.obj' and texture file is 'filename_isomap.png'.
     * @return true = succeed to initialize, false = There are some error.
     */
    bool capture_mesh_and_texture(const char* path, const char* filename);


    /**
     * @brief Get projective matrix in current capture.
     *
     * Get the estimated projective matrix with current face image.
     *
     * @param[out] mat 16-elements array. It's column-major form for OpenGL.
     * @return true = succeed to get, false = There are some error.
     */
    bool get_projective_mat(float* mat);
    
    
    /**
     * @brief Get modelview matrix (rotation and translation of camera) in current capture.
     *
     * Get the estimated modelview matrix with current face image.
     *
     * @param[out] mat 16-elements array. It's column-major form for OpenGL.
     * @return true = succeed to get, false = There are some error.
     */
    bool get_modelview_mat(float* mat);
    
    
    /**
     * @brief Get the iso mapped texture.
     *
     * Get the texture data mapped on fitted facial mesh. It's not merged one.
     *
     * @param[out] width  Width of texture image.
     * @param[out] height Height of texture image.
     * @return buffer for texture. Texture format is BGRA and origin is Left-Bottom for Unity3D and OpenGL. NULL means internal error.
     */
    unsigned char* get_current_iso_map_texture(int* width, int* height);
    
    
    
    
    bool get_profile_face_rect( unsigned char* image_for_unity,
                                int width, int height,
                                int* rect_x, int* rect_y,
                                int* rect_width, int* rect_height);
    

    
    /**
     * @brief Clean up face scanner instance and memory.
     * It's resetting all features. So you have call init_face_scanner() to begin again.
     */
    void clean_up_face_scanner();

    
    /**
     * @brief Get the landmark locatino on current face.
     *
     * Get the landmark of face. It follows the rule of iBug's facial point annotation rule. iBug's facal landmark index begins from 1, not 0. But we'll store them into array starting from 0 for codiinig convenstion, not 1. See the https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/
     *
     * @param[out] verties_on_image  It will store the verties in this order, [X0,Y0, X1, Y1...]. It'll keep the order of iBug's annotation.
     * @param[out] length_of_vertices_on_image how many verticies are in the array.
     * @return true is succeed to get face landmark. false has error.
     */
    bool get_face_landmark_location(float* verties_on_image, int* length_of_vertices_on_image);

    
    /**
     * @brief Get the landmark in 3D on current 4D face mesh.
     *
     * Get the 3D landmark on current 4D face mesh. It follows the rule of iBug's facial point annotation rule.  
     * iBug's facal landmark index begins from 1, not 0. But we'll store them into array starting from 0 for
     * codiinig convenstion, not 1. See the https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/
     *
     *  In FaceScanner/PluginSource/Data/ibug2did.txt, it shows how 2D face landmarks are mached on 3D verticies of 4D face.
     *
     * @param[out] verties_on_mesh  It will store the verties in this order, [X0,Y0,Z0, X1,Y1,Z1...]. It'll keep the order of iBug's annotation.
     * @param[out] number_of_vertices_on_mesh how many verticies are in the array.
     * @return true is succeed to get face landmark. false has error.
     */

    bool get_face_landmark_in_3D_on_4D_face_mesh(float* verties_on_mesh, int* number_of_vertices_on_mesh);
    
    
    // To-Do : will write document.
    bool get_uv_of_vertex_for_unity( int index_of_vertex, float* u, float* v );

    /*
    typedef void (*FuncPtr)( const char * );
    FuncPtr Debug;
    void SetDebugFunction( FuncPtr fp )
    {
        Debug = fp;
    }*/

    
#ifdef __cplusplus
}
#endif


#endif /* face_scanner_h */
