//
//  face_scanner.cpp
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 3. 3..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//

#include "helpers.hpp"


#include "eos/core/Landmark.hpp"
#include "eos/core/LandmarkMapper.hpp"
#include "eos/fitting/fitting.hpp"
#include "eos/fitting/orthographic_camera_estimation_linear.hpp"
#include "eos/fitting/contour_correspondence.hpp"
#include "eos/fitting/closest_edge_fitting.hpp"
#include "eos/fitting/RenderingParameters.hpp"
#include "eos/render/utils.hpp"
#include "eos/render/render.hpp"
#include "eos/render/texture_extraction.hpp"

#include "rcr/model.hpp"
#include "cereal/cereal.hpp"

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Eigen/Dense"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/tracking.hpp>


#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"

#include "unity_util.h"
#include "face_scanner.h"
#include "head_scanner.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>



using namespace eos;

using cv::Mat;
using cv::Vec2f;
using cv::Vec3f;
using cv::Vec4f;
using cv::Scalar;
using cv::Rect;
using std::cout;
using std::endl;
using std::vector;
using std::string;

//=========================================================================================//
// Face Scanner properties set.
//=========================================================================================//
typedef struct FaceScanner{
    bool find_face = false;
    cv::CascadeClassifier face_cascade;
    cv::CascadeClassifier profile_face_cascade;
    rcr::LandmarkCollection<Vec2f> current_landmarks;
    Rect current_facebox;
    rcr::detection_model rcr_model;
    WeightedIsomapAveraging isomap_averaging; // merge all triangles that are facing <60 towards the camera
    PcaCoefficientMerging pca_shape_merging;
    morphablemodel::MorphableModel morphable_model;
    core::LandmarkMapper landmark_mapper;
    fitting::ModelContour model_contour;
    fitting::ContourLandmarks ibug_contour;
    std::vector<morphablemodel::Blendshape> blendshapes;
    morphablemodel::EdgeTopology edge_topology;
    vector<float> shape_coefficients, blendshape_coefficients;
    vector<cv::Vec3f> normals;
    Mat face_scanner_isomap;
    Mat merged_isomap;
    Mat head_mask_image;
    float face_scanner_projection_mat[16];
    float face_scanner_modelview_mat[16];
    std::vector<cv::Point> face_scanner_head_contour;
    int image_width;
    int image_height;
}FACESCANNER_PROPERTIES;


FACESCANNER_PROPERTIES* g_face_scanner = NULL;


//=========================================================================================//
IplImage* create_IplImage(unsigned char*  current_image, int width, int height )
{
    
    IplImage* src_img = cvCreateImageHeader(cvSize(width, height), IPL_DEPTH_8U, 4);
    cvSetData(src_img, current_image, src_img->widthStep);
    return src_img;
}

//=========================================================================================//
bool detect_and_track_face(unsigned char*  current_image, int width, int height )
{
    //Have to configure FaceScanner properties.
    if( !g_face_scanner ){
        cout << "Please initialize face scanner 1st by calling init_face_scanner()." << endl;
        return false;
    }
    
    // Convert texture2D to cv::Mat.
    IplImage* cur_ipl_image = create_IplImage(current_image, width, height);
    cv::Mat frame(cur_ipl_image);
    cv::Mat backup_frame = frame.clone();
    cv::cvtColor(frame, frame, cv::COLOR_BGRA2RGB);
    
    g_face_scanner->image_width = width;
    g_face_scanner->image_height = height;
    
    
    //Flip image to convert origin from OpenGL(Left-bottom) to OpenCV's image coordinate. (Left-Top)
    cv::flip(frame, frame, 0);
    
    //TEST
    //imwrite("test.png", frame);
    
    //Make back up.
    Mat unmodified_frame = frame.clone();
    
    // We do a quick check if the current face's width is <= 50 pixel. If it is, we re-initialise the tracking with the face detector.
    if (g_face_scanner->find_face && get_enclosing_bbox(rcr::to_row(g_face_scanner->current_landmarks)).width <= 50) {
        cout << "Reinitialising because the face bounding-box width is <= 50 px" << endl;
        g_face_scanner->find_face = false;
    }

    if( !g_face_scanner->find_face){
        
        // Run the face detector and obtain the initial estimate using the mean landmarks:
        vector<Rect> detected_faces;
        g_face_scanner->face_cascade.detectMultiScale(unmodified_frame, detected_faces, 1.2, 2, 0, cv::Size(110, 110));

        
        // If no face in the screen, return false;
        if( detected_faces.empty() ){
            g_face_scanner->find_face = false;
            return g_face_scanner->find_face;
        }
        
        
        cv::rectangle(frame, detected_faces[0], { 255, 0, 0 });
        
        // Rescale the V&J facebox to make it more like an ibug-facebox:
        // (also make sure the bounding box is square, V&J's is square)
        Rect ibug_facebox = rescale_facebox(detected_faces[0], 0.85, 0.2);
        
        g_face_scanner->current_landmarks = g_face_scanner->rcr_model.detect(unmodified_frame, ibug_facebox);
        //rcr::draw_landmarks(frame,  g_face_scanner->current_landmarks, { 0, 0, 255 }); // red, initial landmarks
        
        g_face_scanner->find_face = true;
        
    }else{
        
        // We already have a face - track and initialise using the enclosing bounding
        // box from the landmarks from the last frame:
        cout<< "Track the face." << endl;
        auto enclosing_bbox = get_enclosing_bbox(rcr::to_row(g_face_scanner->current_landmarks));
        enclosing_bbox = make_bbox_square(enclosing_bbox);
        g_face_scanner->current_landmarks =  g_face_scanner->rcr_model.detect(unmodified_frame, enclosing_bbox);
        //rcr::draw_landmarks(frame,  g_face_scanner->current_landmarks, { 255, 0, 0 }); // blue, the new optimised landmarks
    }
    
    
    // Fit the 3DMM:
    fitting::RenderingParameters rendering_params;
    vector<Vec2f> image_points;
    render::Mesh mesh;
    std::tie(mesh, rendering_params)
        = fitting::fit_shape_and_pose(
                        g_face_scanner->morphable_model,  g_face_scanner->blendshapes,
                        rcr_to_eos_landmark_collection( g_face_scanner->current_landmarks),
                        g_face_scanner->landmark_mapper, unmodified_frame.cols, unmodified_frame.rows,
                        g_face_scanner->edge_topology,  g_face_scanner->ibug_contour,
                        g_face_scanner->model_contour, 3, 5, 15.0f, boost::none,
                        g_face_scanner->shape_coefficients,
                        g_face_scanner->blendshape_coefficients,
                        image_points
                        );
    
    // TEST for mapped image points.
    /*
    cv::Mat debug_img = unmodified_frame.clone();
    for(int i=0; i<image_points.size(); i++){
        cv::circle(debug_img,
                   cv::Point( (int)image_points[i][0], (int)image_points[i][1] ),
                   3, cv::Scalar(255, 0, 0) );
    }
    cv::imwrite("image_points.png", debug_img);
    */
    
    
    // Extract the texture using the fitted mesh from this frame:
    Mat affine_cam = fitting::get_3x4_affine_camera_matrix(rendering_params, frame.cols, frame.rows);
    Mat isomap = render::extract_texture(
                mesh, affine_cam, unmodified_frame,
                g_face_scanner->normals, true,
                render::TextureInterpolation::NearestNeighbour, 512);
    g_face_scanner->face_scanner_isomap = isomap.clone(); // Make back up of iso image.
    
    // Fill modelview matrix and projection matrix for current face image.
    glm::mat4x4 modelview = rendering_params.get_modelview();
    glm::mat4x4 projection = rendering_params.get_projection();
    
    for(int i=0; i<16; ++i){
        g_face_scanner->face_scanner_modelview_mat[i] = glm::value_ptr(modelview)[i];
        g_face_scanner->face_scanner_projection_mat[i] = glm::value_ptr(projection)[i];
    }
    
    // Merge the isomaps - add the current one to the already merged ones:
    g_face_scanner->merged_isomap =  g_face_scanner->isomap_averaging.add_and_merge(isomap);
    
    
    // Same for the shape:
    g_face_scanner->shape_coefficients
        = g_face_scanner->pca_shape_merging.add_and_merge( g_face_scanner->shape_coefficients);
    auto merged_shape
        = g_face_scanner->morphable_model.get_shape_model().draw_sample( g_face_scanner->shape_coefficients) +
                morphablemodel::to_matrix( g_face_scanner->blendshapes) * Mat( g_face_scanner->blendshape_coefficients);
    render::Mesh merged_mesh
        = morphablemodel::sample_to_mesh(
                             merged_shape,
                             g_face_scanner->morphable_model.get_color_model().get_mean(),
                             g_face_scanner->morphable_model.get_shape_model().get_triangle_list(),
                             g_face_scanner->morphable_model.get_color_model().get_triangle_list(),
                             g_face_scanner->morphable_model.get_texture_coordinates()
                             );
    
    
    // Release the image.
    cvReleaseImageHeader(&cur_ipl_image);
    
    return  g_face_scanner->find_face;
}

//=========================================================================================//
bool find_face_landmark(unsigned char* current_image, int width, int height)
{
    //Have to configure FaceScanner properties.
    if( !g_face_scanner ){
        cout << "Please initialize face scanner 1st by calling init_face_scanner()." << endl;
        return false;
    }
    
    // Convert texture2D to cv::Mat.
    IplImage* cur_ipl_image = create_IplImage(current_image, width, height);
    cv::Mat frame(cur_ipl_image);
    cv::Mat backup_frame = frame.clone();
    cv::cvtColor(frame, frame, cv::COLOR_BGRA2RGB);
    
    //Flip image to convert origin from OpenGL(Left-bottom) to OpenCV's image coordinate. (Left-Top)
    cv::flip(frame, frame, 0);
    
    //Make back up.
    Mat unmodified_frame = frame.clone();
    
    // We do a quick check if the current face's width is <= 50 pixel. If it is, we re-initialise the tracking with the face detector.
    if (g_face_scanner->find_face && get_enclosing_bbox(rcr::to_row(g_face_scanner->current_landmarks)).width <= 50) {
        cout << "Reinitialising because the face bounding-box width is <= 50 px" << endl;
        g_face_scanner->find_face = false;
    }
    
    if( !g_face_scanner->find_face){
        
        // Run the face detector and obtain the initial estimate using the mean landmarks:
        vector<Rect> detected_faces;
        g_face_scanner->face_cascade.detectMultiScale(unmodified_frame,
                                                      detected_faces, 1.1, 2,
                                                      0,
                                                      cv::Size(300, 300)
                                                      );
        
        
        // If no face in the screen, return false;
        if( detected_faces.empty() ){
            g_face_scanner->find_face = false;
            return g_face_scanner->find_face;
        }
        
        // Rescale the V&J facebox to make it more like an ibug-facebox:
        // (also make sure the bounding box is square, V&J's is square)
        Rect ibug_facebox = rescale_facebox(detected_faces[0], 0.85, 0.2);
        
        g_face_scanner->current_landmarks = g_face_scanner->rcr_model.detect(unmodified_frame, ibug_facebox);
        g_face_scanner->find_face = true;
        
    }else{
        
        // We already have a face - track and initialise using the enclosing bounding
        // box from the landmarks from the last frame:
        auto enclosing_bbox = get_enclosing_bbox(rcr::to_row(g_face_scanner->current_landmarks));
        enclosing_bbox = make_bbox_square(enclosing_bbox);
        g_face_scanner->current_landmarks =  g_face_scanner->rcr_model.detect(unmodified_frame, enclosing_bbox);
    }
    
    // Release the image.
    cvReleaseImageHeader(&cur_ipl_image);
    
    return  g_face_scanner->find_face;
}



//=========================================================================================//
bool find_face_rect(unsigned char* cur_image_for_unity,
                    int width, int height,
                    int* rect_x, int* rect_y, int* rect_width, int* rect_height)
{
    //Have to configure FaceScanner properties.
    if( !g_face_scanner ){
        cout << "Please initialize face scanner 1st by calling init_face_scanner()." << endl;
        return false;
    }
    
    cv::Mat frame = convert_texture2D_to_mat(cur_image_for_unity, width, height);
    
    
    //Make back up.
    Mat unmodified_frame = frame.clone();
    
    // We do a quick check if the current face's width is <= 50 pixel. If it is, we re-initialise the tracking with the face detector.
    if (g_face_scanner->find_face && get_enclosing_bbox(rcr::to_row(g_face_scanner->current_landmarks)).width <= 50) {
        cout << "Reinitialising because the face bounding-box width is <= 50 px" << endl;
        g_face_scanner->find_face = false;
    }
    
    
    // Run the face detector and obtain the initial estimate using the mean landmarks:
    vector<Rect> detected_faces;
    g_face_scanner->face_cascade.detectMultiScale(unmodified_frame,
                                                  detected_faces, 1.1, 2,
                                                  0,
                                                  cv::Size(300, 300)
                                                  );
    
    // If no face in the screen, return false;
    if( detected_faces.empty() ){
        g_face_scanner->find_face = false;
        return g_face_scanner->find_face;
    }
        
    // Rescale the V&J facebox to make it more like an ibug-facebox:
    // (also make sure the bounding box is square, V&J's is square)
    Rect ibug_facebox = rescale_facebox(detected_faces[0], 0.85, 0.2);
        
    *rect_x      = ibug_facebox.x;
    *rect_y      = ibug_facebox.y;
    *rect_width  = ibug_facebox.width;
    *rect_height = ibug_facebox.height;
    
    g_face_scanner->find_face = true;
    return g_face_scanner->find_face;
    
}



//=========================================================================================//
bool quick_find_face_landmark(unsigned char* prev_image_for_unity,
                              unsigned char* cur_image_for_unity,
                              int width, int height, int window_size)
{
    
    //Have to configure FaceScanner properties.
    if( !g_face_scanner ){
        cout << "Please initialize face scanner 1st by calling init_face_scanner()." << endl;
        return false;
    }
    
    cv::Mat frame = convert_texture2D_to_mat(cur_image_for_unity, width, height);
        
    
    //Make back up.
    Mat unmodified_frame = frame.clone();
    
    // We do a quick check if the current face's width is <= 50 pixel. If it is, we re-initialise the tracking with the face detector.
    if (g_face_scanner->find_face && get_enclosing_bbox(rcr::to_row(g_face_scanner->current_landmarks)).width <= 50) {
        cout << "Reinitialising because the face bounding-box width is <= 50 px" << endl;
        g_face_scanner->find_face = false;
    }
    
    if( !g_face_scanner->find_face){
        
        // Run the face detector and obtain the initial estimate using the mean landmarks:
        vector<Rect> detected_faces;
        g_face_scanner->face_cascade.detectMultiScale(unmodified_frame,
                                                      detected_faces, 1.1, 2,
                                                      0,
                                                      cv::Size(300, 300)
                                                      );
        
        // If no face in the screen, return false;
        if( detected_faces.empty() ){
            g_face_scanner->find_face = false;
            return g_face_scanner->find_face;
        }
        
        // Rescale the V&J facebox to make it more like an ibug-facebox:
        // (also make sure the bounding box is square, V&J's is square)
        Rect ibug_facebox = rescale_facebox(detected_faces[0], 0.85, 0.2);
        
        g_face_scanner->current_landmarks = g_face_scanner->rcr_model.detect(unmodified_frame, ibug_facebox);
        g_face_scanner->find_face = true;
        
    }else
    {
        if (NULL == prev_image_for_unity)
        {
            
            // We already have a face - track and initialise using the enclosing bounding
            // box from the landmarks from the last frame:
            auto enclosing_bbox = get_enclosing_bbox(rcr::to_row(g_face_scanner->current_landmarks));
            enclosing_bbox = make_bbox_square(enclosing_bbox);
            g_face_scanner->current_landmarks =  g_face_scanner->rcr_model.detect(unmodified_frame, enclosing_bbox);
            
        }
        else{

            // Use optical flow to reduce operation time.
            cv::Mat prev_frame = convert_texture2D_to_mat(prev_image_for_unity, width, height);
            cv::Mat gray, prev_gray;
            
            cv::cvtColor(frame, gray, CV_RGB2GRAY);
            cv::cvtColor(prev_frame, prev_gray, CV_RGB2GRAY);
            
            std::vector<cv::Point2f> prev_landmark;
            std::vector<cv::Point2f> cur_landmark;
            
            
            for(int j=0; j< static_cast<int>(g_face_scanner->current_landmarks.size()); ++j )
            {
                float x = g_face_scanner->current_landmarks[j].coordinates[0];
                float y = g_face_scanner->current_landmarks[j].coordinates[1];
                prev_landmark.push_back( cv::Point2f(x,y) );
            }
            
            std::vector<uchar> status;
            std::vector<float> err;
            
            
            cv::calcOpticalFlowPyrLK(prev_gray, gray,
                                     prev_landmark, cur_landmark,
                                     status, err,
                                     cv::Size(window_size, window_size) );
            
            
            float reliability = 0.0;
            for(int j=0; j< static_cast<int>(g_face_scanner->current_landmarks.size()); ++j )
            {
                if(status[j])
                {
                    reliability += 1.0f;
                    g_face_scanner->current_landmarks[j].coordinates[0] = cur_landmark[j].x;
                    g_face_scanner->current_landmarks[j].coordinates[1] = cur_landmark[j].y;
                }
                
            }
            
            // If results are not reliable, just try to detect face again.
            reliability = reliability/static_cast<float>(g_face_scanner->current_landmarks.size());
            if( reliability < 1.0f)
            {
                std::cout << "Reinitialize..." << std::endl;
                g_face_scanner->find_face = false;
            }
        }
        
    }
    
    
    return  g_face_scanner->find_face;
}

//=========================================================================================//
bool capture_mesh_and_texture(const char* path, const char* filename )
{
    
    
    std::string filePath(path);
    std::string fileName(filename);

    std::string fullPathTexture = filePath+"/"+fileName+".isomap.png";
    std::string fullPathObj = filePath+"/"+fileName+".obj";


    // save an obj + current merged isomap to the disk:
    render::Mesh neutral_expression = morphablemodel::sample_to_mesh(
        g_face_scanner->morphable_model.get_shape_model().draw_sample(g_face_scanner->shape_coefficients),
        g_face_scanner->morphable_model.get_color_model().get_mean(),
        g_face_scanner->morphable_model.get_shape_model().get_triangle_list(),
        g_face_scanner->morphable_model.get_color_model().get_triangle_list(),
        g_face_scanner->morphable_model.get_texture_coordinates()
    );
    
    try {
        render::write_textured_obj(neutral_expression, fullPathObj );
        cv::imwrite(fullPathTexture,  g_face_scanner->merged_isomap);
    }
    catch (std::ifstream::failure e) {
        std::cerr << "Exception opening/reading/closing file\n";
        return false;
    }
    return true;
}

//=========================================================================================//
unsigned char* get_current_iso_map_texture(int* width, int* height){
    
    if(!g_face_scanner)
    {
        cout << "Not initialized. Call init_face_scanner(). " << endl;
        return NULL;
    }
    
    if(!g_face_scanner->find_face)
    {
        cout << "Cannot find face in current image. Do detect_and_track_face() again." << endl;
        return NULL;
    }
    
    
    cv::Mat& isomap =  g_face_scanner->face_scanner_isomap;
    *width = isomap.cols;
    *height = isomap.rows;
    
    
    if ( 4 == isomap.channels() )
    {
        cv::flip(isomap, isomap, 0); //Flip image to emulate OpenGL's origin. (Left-Bottom)
        return isomap.ptr();
    }else if( 3 == isomap.channels() )
    {
        cv::Mat newImage = cv::Mat( isomap.rows, isomap.cols, CV_8UC4 );
        cv::flip(isomap, isomap, 0); //Flip image to emulate OpenGL's origin. (Left-Bottom)
        return isomap.ptr();
    }
    
    return NULL;
}


//=========================================================================================//
vector<string> split(string str, char delimiter)
{
    vector<string> internal;
    std::stringstream ss(str);
    string tok;
    
    while(getline(ss, tok, delimiter))
    {
        internal.push_back(tok);
    }
    
    return internal;
}

//=========================================================================================//

int init_face_scanner(const char* model_file, const char* mappings_file,
                      const char* contour_file,const char* landmarkdetector_file,
                      const char* facedetector_file, const char* profile_facedetector_file,
                      const char* blendshapes_file, const char* edgetopology_file,
                      const char* normals_file)
{
    
    std::string modelfile(model_file);
    std::string mappingsfile(mappings_file);
    std::string contourfile(contour_file);
    std::string landmarkdetector(landmarkdetector_file);
    std::string facedetectorfile(facedetector_file);
    std::string normalsfile(normals_file);
   
    
    if(g_face_scanner)
        return EXIT_FAILURE;
    
    // Initialize face tracker properties.
    g_face_scanner = new FACESCANNER_PROPERTIES;
    g_face_scanner->find_face = false;
    g_face_scanner->isomap_averaging.set_merge_threshold(60.0); // merge all triangles that are facing <60 towards the camera

    
    // Load the Morphable Model and the LandmarkMapper:
    g_face_scanner->morphable_model = morphablemodel::load_model(modelfile);
    g_face_scanner->landmark_mapper = mappingsfile.empty() ? core::LandmarkMapper() : core::LandmarkMapper(mappingsfile);
    
    g_face_scanner->model_contour = contourfile.empty() ? fitting::ModelContour() : fitting::ModelContour::load(contourfile);
    g_face_scanner->ibug_contour = fitting::ContourLandmarks::load(mappingsfile);
    

    // Load the landmark detection model:
    try {
        g_face_scanner->rcr_model = rcr::load_detection_model(landmarkdetector);
    }
    catch (const cereal::Exception& e) {
        std::cout << "Error reading the RCR model " << landmarkdetector << ": " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    
    // Load the face detector from OpenCV:
    if (!g_face_scanner->face_cascade.load( std::string(facedetector_file) ) )
    {
        std::cout << "Error loading the face detector " << facedetectorfile << "." << std::endl;
        return EXIT_FAILURE;
    }

    // Load the profile face detector from OpenCV:
    if (!g_face_scanner->profile_face_cascade.load( std::string(profile_facedetector_file) ) )
    {
        std::cout << "Error loading the face detector " << facedetectorfile << "." << std::endl;
        return EXIT_FAILURE;
    }
    
    // Load Blendshape and edge topology information.
    std::string blendshapesfile(blendshapes_file);
    std::string edgetopologyfile(edgetopology_file);
    g_face_scanner->blendshapes = morphablemodel::load_blendshapes(blendshapesfile);
    g_face_scanner->edge_topology = morphablemodel::load_edge_topology(edgetopologyfile);

    
    // Clear face_scanner_head_contour
    g_face_scanner->face_scanner_head_contour.clear();

    
    // Load Smoothed Mesh Normals
    // Debug("Loading Normals");
    // std::fprintf(stderr, "UGO3D\n");
    // std::cout << "The normal file path is = "<< normalsfile << std::endl;
    std::ifstream f (normalsfile);
    if (f.is_open())
    {
        int c = 0;
        string line;
        while ( getline (f, line) )
        {
//            Debug(line.c_str());
            
            vector<string> vals = split(line, ' ');
            float x = std::atof(vals[1].c_str());
            float y = std::atof(vals[2].c_str());
            float z = std::atof(vals[3].c_str());
            g_face_scanner->normals.push_back(cv::Vec3f{x, y, z});
            
            c++;
        }
        f.close();
    }else
    {
        cout << "No file:"<< normalsfile << endl;
    }
    
    
    //cout << "The size of normal = "<< g_face_scanner->normals.size() << endl;
    
    return EXIT_SUCCESS;
}


bool get_modelview_mat(float* mat)
{
    if(!mat){
        cout << "Prepare the float array[16] to store column-major matrix data." << endl;
        return false;
    }

    
    if(!g_face_scanner)
    {
        cout << "Not initialized. Call init_face_scanner(). " << endl;
        return false;
    }
    
    if(!g_face_scanner->find_face)
    {
        cout << "Cannot find face in current image. Do detect_and_track_face() again." << endl;
        return false;
    }
    
    for(int i=0; i<16; ++i){
        mat[i]= g_face_scanner->face_scanner_modelview_mat[i];
    }
    
    return true;
}



bool get_projective_mat(float* mat)
{
    if(!mat){
        cout << "Prepare the float array[16] to store column-major matrix data." << endl;
        return false;
    }
    
    if(!g_face_scanner)
    {
        cout << "Not initialized. Call init_face_scanner(). " << endl;
        return false;
    }
    
    if(!g_face_scanner->find_face)
    {
        cout << "Cannot find face in current image. Do detect_and_track_face() again." << endl;
        return false;
    }
    
    for(int i=0; i<16; ++i){
        mat[i]= g_face_scanner->face_scanner_projection_mat[i];
    }
    
    return true;
}

bool get_head_contour(float* array )
{
    if(!array){
        cout << "Prepare the float array to store point data." << endl;
        return false;
    }
    
    if(!g_face_scanner)
    {
        cout << "Not initialized. Call init_face_scanner(). " << endl;
        return false;
    }
    
    if(!g_face_scanner->find_face)
    {
        cout << "Cannot find face in current image. Do detect_and_track_face() again." << endl;
        return false;
    }
    
    if( g_face_scanner->face_scanner_head_contour.empty() )
        return false;
    
    for(int i=0; i<g_face_scanner->face_scanner_head_contour.size(); ++i)
    {
        array[2*i] = g_face_scanner->face_scanner_head_contour[i].x;
        array[2*i+1] = g_face_scanner->face_scanner_head_contour[i].y;
    }
    
    return true;
}


int get_head_contour_size()
{
    
    if(!g_face_scanner)
    {
        cout << "Not initialized. Call init_face_scanner(). " << endl;
        return 0;
    }
    
    if(!g_face_scanner->find_face)
    {
        cout << "Cannot find face in current image. Do detect_and_track_face() again." << endl;
        return 0;
    }
    
    if( g_face_scanner->face_scanner_head_contour.empty()  )
        return 0;
    
    return g_face_scanner->face_scanner_head_contour.size();
}







bool get_face_landmark_location(float* verties_on_image, int* length_of_vertices_on_image)
{
    
    if(!g_face_scanner)
    {
        cout << "Not initialized. Call init_face_scanner(). " << endl;
        return false;
    }
    
    if(!g_face_scanner->find_face)
    {
        cout << "Cannot find face in current image. Do detect_and_track_face() again." << endl;
        return false;
    }
    
    
    *length_of_vertices_on_image = static_cast<int>( g_face_scanner->current_landmarks.size() );
    
    for(int i=0; i<g_face_scanner->current_landmarks.size(); ++i)
    {
        int index = atoi( g_face_scanner->current_landmarks[i].name.c_str() );
        cv::Vec2f& pt = g_face_scanner->current_landmarks[i].coordinates;
        
        //iBug's facal landmark index begins from 1, not 0. But we'll store them into array starting from 0 for codiinig convenstion, not 1. 
        verties_on_image[2*(index-1)] = pt[0];
        verties_on_image[2*(index-1)+1] = pt[1];
    }
    
    return g_face_scanner->find_face;
}


bool get_face_landmark_in_3D_on_4D_face_mesh(float* verties_on_mesh, int* number_of_vertices_on_mesh)
{
    
    int landmark_vertices_indices []
        = { -1	,	-1	,	-1	,	-1	,	-1	,	-1	,	-1	,	-1	,	33	,	-1	,
            -1	,	-1	,	-1	,	-1	,	-1	,	-1	,	-1	,	225	,	229	,	233	,
            2086,	157	,	590	,	2091,	666	,	662	,	658	,	2842,	379	,	272	,
            114	,	100	,	2794,	270	,	2797,	537	,	177	,	172	,	191	,	181	,
            173	,	174	,	614	,	624	,	605	,	610	,	607	,	606	,	398	,	315	,
            413	,	329	,	825	,	736	,	812	,	841	,	693	,	411	,	264	,	431	,
            -1	,	416	,	423	,	828	,	-1	,	817	,	442	,	404	};
    
    if(!g_face_scanner)
    {
        std::cerr << "Not initialized. Call init_face_scanner(). " << std::endl;
        return false;
    }
    
    if(!g_face_scanner->find_face)
    {
        std::cerr << "Cannot find face in current image. Do detect_and_track_face() again." << std::endl;
        return false;
    }
    
    // Get the number of landmark. Fixed as 68.
    *number_of_vertices_on_mesh = g_face_scanner->current_landmarks.size();
    
    // Get current mesh data.
    render::Mesh neutral_expression
        = morphablemodel::sample_to_mesh(g_face_scanner->morphable_model.get_shape_model().draw_sample(g_face_scanner->shape_coefficients),
                                         g_face_scanner->morphable_model.get_color_model().get_mean(),
                                         g_face_scanner->morphable_model.get_shape_model().get_triangle_list(),
                                         g_face_scanner->morphable_model.get_color_model().get_triangle_list(),
                                         g_face_scanner->morphable_model.get_texture_coordinates()
                                         );
    
    // Find 3D vertex matched on 2D landmark.
    for(int i=0; i<g_face_scanner->current_landmarks.size(); ++i)
    {
        int index = landmark_vertices_indices[i];
        glm::tvec4<float> pt;
        if (index > 0)
            pt = neutral_expression.vertices[index]; // Matched on 3D vertex.
        else
            pt = glm::tvec4<float>( -1, -1, -1, -1 ); // Not matched on 3D vertex.

        verties_on_mesh[3*i]   = pt[0];
        verties_on_mesh[3*i+1] = pt[1];
        verties_on_mesh[3*i+2] = pt[2];
    }
    
    return g_face_scanner->find_face;
}



bool  get_profile_face_rect(unsigned char* image_for_unity,
                            int width, int height,
                            int* rect_x, int* rect_y,
                            int* rect_width, int* rect_height)
{
    
    if(!g_face_scanner)
    {
        std::cerr << "Not initialized. Call init_face_scanner(). " << std::endl;
        return false;
    }
    
    cv::Mat image = convert_texture2D_to_mat(image_for_unity, width, height);
    cv::Mat gray_image = cv::Mat( height, width, CV_8UC1);
    cv::cvtColor(image, gray_image, cv::COLOR_RGB2GRAY);

    std::vector<cv::Rect> face_rect_array;
    g_face_scanner->profile_face_cascade.detectMultiScale(gray_image, face_rect_array);
   
    
    if( face_rect_array.empty() )
    {
        return false;
    }
    
    *rect_x = face_rect_array[0].x;
    *rect_y = face_rect_array[0].y;
    *rect_width = face_rect_array[0].width;
    *rect_height = face_rect_array[0].height;
    
#ifdef DEBUG
    std::cout<< "Face rect: (" << *rect_x <<","<< *rect_y<<","<< *rect_width<<","<< *rect_height<<")"<<std::endl;
#endif
    
    
    return true;
}


bool get_uv_of_vertex_for_unity( int index_of_vertex, float* u, float* v )
{
    
    if(!g_face_scanner)
    {
        std::cerr << "Not initialized. Call init_face_scanner(). " << std::endl;
        return false;
    }
    
    if(!g_face_scanner->find_face)
    {
        std::cerr << "Cannot find face in current image. Do detect_and_track_face() again." << std::endl;
        return false;
    }
    
    std::vector<std::array<int, 3>> vertex_list =
        g_face_scanner->morphable_model.get_shape_model().get_triangle_list();
    
    if( index_of_vertex >= static_cast<int>(vertex_list.size())   )
        return false;
    
    
    
    int width = g_face_scanner->image_width;
    int height = g_face_scanner->image_height;
    
    glm::mat4 modelview = glm::make_mat4(g_face_scanner->face_scanner_projection_mat);
    glm::mat4 projective = glm::make_mat4(g_face_scanner->face_scanner_projection_mat);
    glm::vec4 viewport(0, 0, width, height);

    glm::vec3 pos =
        glm::project({vertex_list[index_of_vertex][0],
                      vertex_list[index_of_vertex][1],
                      vertex_list[index_of_vertex][2]},
                      modelview, projective, viewport );

    float u_value = pos.x/float(width);
    float v_value = pos.x/float(height);
    
    *u = u_value;
    *v = v_value;
    
    return true;
}





void clean_up_face_scanner()
{
    if(!g_face_scanner)
        return;
    
    delete g_face_scanner;
    g_face_scanner = NULL;
}













