//
//  video_tracker.hpp
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 5. 29..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//

#ifndef video_tracker_hpp
#define video_tracker_hpp

#include <stdio.h>



#ifdef __cplusplus
extern "C" {
#endif
    
    bool unity_init_video_tracker( const float distance_limit, const int window_size);
    
    bool unity_destroy_video_tracker();
    
    bool unity_track_face_landmark(int idx,
                                     unsigned char* cur_img_for_unity,
                                     unsigned char* prev_img_for_unity,
                                     int width, int height,
                                     float* current_key_face_landmark,
                                     int* number_of_face_landmark,
                                     float* reliability = NULL,
                                     bool* using_optical_flow = NULL);

    
#ifdef __cplusplus
}  // extern "C" 
#endif
        


#endif /* video_tracker_hpp */
