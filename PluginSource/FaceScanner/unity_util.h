//
//  unity_util.h
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 5. 11..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//

#ifndef unity_util_h
#define unity_util_h

// OpenCV headers
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using cv::Mat;
using cv::Size;

cv::Mat convert_texture2D_to_mat(unsigned char* image_from_unity, int width, int height);


#endif /* unity_util_h */


