//
//  head_scanner.h
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 3. 18..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//

#ifndef head_scanner_h
#define head_scanner_h

//VCG headers
#include <vcg/complex/complex.h>
#include <vcg/complex/algorithms/create/marching_cubes.h>
#include <vcg/complex/algorithms/create/mc_trivial_walker.h>
#include <wrap/io_trimesh/export_obj.h>


// OpenCV headers
#include "opencv2/core/core.hpp"


// Standard C++ headers
#include <vector>
#include <algorithm>
#include <iterator>
#include <cassert>



// VCG data structure and classes.
using namespace vcg;

class MyVertex; class MyEdge; class MyFace;
struct MyUsedTypes : public vcg::UsedTypes<vcg::Use<MyVertex>   ::AsVertexType,
vcg::Use<MyEdge>     ::AsEdgeType,
vcg::Use<MyFace>     ::AsFaceType>{};
class MyVertex  : public vcg::Vertex< MyUsedTypes, vcg::vertex::Coord3f, vcg::vertex::Normal3f >{};
class MyFace    : public vcg::Face<   MyUsedTypes, vcg::face::FFAdj,  vcg::face::VertexRef, vcg::face::BitFlags > {};
class MyEdge    : public vcg::Edge<   MyUsedTypes> {};
class MyMesh    : public vcg::tri::TriMesh< std::vector<MyVertex>, std::vector<MyFace> , std::vector<MyEdge>  > {};

#ifdef __cplusplus
extern "C" {
#endif
    

// Exposed functions to Unity.
//========================================

/**
 * @brief Get foreground mask with webcam texture image by grab cut.
 *
 *  Do GrabCut with given mask image and current image.
 *
 * Mask is image with the value below.
 *  GC_PR_BGD = 2  most probably background
 *  GC_PR_FGD = 3  most probably foreground
 *
 * @param[in] webcam_texture webcam texture image from unity
 * @param[in] width width of image
 * @param[in] height height of image
 * @param[in] iter Accracy of grabcut operation. Can get sloer ith bigger number.
 * @param[out] mask_for_unity Mask biinary image. Origin is [Bottom,Left] for Unity.
 */
void refine_mask_by_grabcut(unsigned char* webcam_texture,
                            int width, int height, int iter,
                            unsigned char* mask_for_unity,
                            bool use_hand_painted_mask = false);
    

/**
 * @brief Generate ellipse mask.
 *
 * Get blob mask image with ellipse mask. See the https://en.wikipedia.org/wiki/Ellipse.
 *
 * Mask is image with the value below.
 *  GC_PR_BGD = 2  most probably background
 *  GC_PR_FGD = 3  most probably foreground
 *
 * @param[in] cx x of ellipse center.
 * @param[in] cy y of ellipse center.
 * @param[in] semi_major_axis Axis of ellipse mask. it's (long axis, short axis)
 * @param[in] semi_minor_axis Axis of ellipse mask. it's (long axis, short axis)
 * @param[in] width width of image
 * @param[in] height height of image
 * @param[out] mask_for_unity Mask binary image. Origin is [Bottom,Left] for Unity.
 */
void get_ellipse_mask(int cx, int cy,
                      int semi_major_axis, int semi_minor_axis,
                      int width, int height, unsigned char* mask_for_unity );
    


/**
 * @brief Generate initial face mask with facial landmark.
 *
 * Fit ellipse on points of chin and draw neck mask.
 *
 * Mask is image with the value below.
 *  GC_PR_BGD = 2  most probably background
 *  GC_PR_FGD = 3  most probably foreground
 *
 * @param[in] face_landmark array of facial landmark points
 * @param[in] length_of_facial_landmark length of landmakr points. Have to be 68.
 * @param[in] width width of image
 * @param[in] height height of image
 * @param[in] tx translate mask in X direction. (origin is top-left.)
 * @param[in] ty translate mask in X direction. (origin is top-left.)
 * @param[out] mask_for_unity Mask binary image. Origin is [Bottom,Left] for Unity.
 */
void get_mask_from_face_landmark(float* face_landmark,
                                   int length_of_face_landmark,
                                   int width, int height,
                                   unsigned char* mask_for_unity,
                                   int tx=0, int ty=0,
                                   int face_width_comp = 5, int face_height_comp = 5,
                                   int face_tx = 0, int face_ty = 0,
                                   int head_width_comp = 5, int head_height_comp = 5,
                                   int head_tx = 0, int head_ty = 0,
                                   int neck_width_comp = 30, int neck_height_comp = 40
                                 );


/**
 * @brief Generate intial left or right face mask with facial landmark.
 *
 * Fit ellipse on points of chin and draw neck mask.
 *
 * Mask is image with the value below.
 *  GC_PR_BGD = 2  most probably background
 *  GC_PR_FGD = 3  most probably foreground
 *
 * @param[in] facial_landmark array of facial landmark points
 * @param[in] length_of_facial_landmark length of landmakr points. Have to be 68.
 * @param[in] width width of image
 * @param[in] height height of image
 * @param[in] is_left_side True means to generate left-side face mask. False means right side. Left side face = the nose is left side.
 * @param[out] mask_for_unity Mask binary image. Origin is [Bottom,Left] for Unity.
 */
void get_side_mask_from_face_landmark(float* facial_landmark,
                                      int length_of_facial_landmark,
                                      int width, int height,
                                      bool is_left_side,
                                      unsigned char* mask_for_unity,
                                      int face_width_comp = 5, int face_height_comp = 5,
                                      int face_tx = 0,         int face_ty = 0,
                                      int head_width_comp = 5, int head_height_comp = 5,
                                      int head_tx = 0,         int head_ty = 0,
                                      int neck_width_comp = 30, int neck_height_comp = 40);


/**
 * @brief Generate ellipse mask by detecting left and right side face.
 *
 * Fit ellipse on points of chin and draw neck mask.
 *
 * Mask is image with the value below.
 *  GC_PR_BGD = 2  most probably background
 *  GC_PR_FGD = 3  most probably foreground
 *
 * @param[in] image_for_unity image buffer from unity (Color32[] of Texture2D)
 * @param[in] width width of image
 * @param[in] height height of image
 * @param[out] mask_for_unity Mask binary image. Origin is [Bottom,Left] for Unity.
 */

bool get_side_face_mask(unsigned char* image_for_unity,
                        int width, int height,
                        unsigned char* mask_for_unity);


/**
 * @brief Get foreground mask by grab cut
 *
 * Build 3D volume points from front and right mask in image coordinates.
 *
 * @param[in] front_mask Mask image of front face image.
 * @param[in] right_mask Mask image of right face image.
 * @param[in] width width of image
 * @param[in] height height of image
 * @param[in] modelview_mat  modelview matrix from 4D Face.
 * @param[in] projective_mat projective matrix from 4D Face.
 * @param[in] file_path File path to store volume data. It'll store 3D data in OBJ format. 
 * @param[in] lod_of_depth How many divide the depth of volume.
 * @param[in] lod_of_top_circle How many divide top unit circle's circumference.
 * @return true, succeed to build new model. fail, fail to build model with current mask.
 */
bool build_head_volume(unsigned char* front_mask,
                       unsigned char* right_mask,
                       int width, int height,
                       float* modelview_mat,
                       float* projective_mat,
                       const char* file_path,
                       int lod_of_depth=16,
                       int lod_of_top_circle=20
                       );

    
//========================================

    
/**
 * @brief Get foreground mask by grab cut
 *
 *  Do GrabCut with given mask image and current image.
 *
 * @param[in] bgr_image buffer pointer of image formatted in BGR.
 * @param[in] width width of image
 * @param[in] height height of image
 * @param[in] iter Accracy of grabcut operation. Can get sloer ith bigger number.
 * @param[out] mask Mask biinary image.
 */
void get_foreground_mask_by_grab_cut(unsigned char* bgr_image, int width, int height, int iter, unsigned char* mask);


/**
 * @brief build 3D points from two silhouetes in image coordinate.
 *
 * Build 3D volume points from front and right mask in image coordinates.
 *
 * @param[in] lod_of_depth How many divide the depth of volume.
 * @param[in] lod_of_top_circle How many divide top unit circle's circumference.
 * @param[out] points_data The point cloud in each depth.
 * @return true, succeed to build new model. fail, fail to build model with current mask.
 */
bool bulld_3d_points_from_two_blob( cv::Mat& front_mask, cv::Mat& right_mask, std::vector< std::vector<cv::Vec3f> >& points_data, int lod_of_depth=16, int lod_of_top_circle=20);

    
/**
 * @brief Get points of unit circle on x-z plan.
 *
 * Get points of unit circle which center is (0,0) and radiius is 1.
 *
 * @param[in] step The number that divide 360 degree. It's the number of points on unit circle too.
 * @param[out] points The points on the unit circle.
 */
void get_unit_circle_points_on_xz_plan( int step, std::vector<cv::Vec3f>& points);
    
    
/**
 * @brief Warp unit circle on x-z plan.
 *
 * Warp unit circle to represent the slice of volume.
 *
 * @param[in, out] points the list of points on the unit circle.
 * @param[in] length_of_axis_x length of axis x.
 * @param[in] length_of_axis_z length of axis z.
 * @param[in] tx translation on axis x.
 * @param[in] tz translation on axis z.
 * @param[in] depth_of_layer_on_y_axis depth level along y axis.
 * @param[out] output_points list of transformed points
 */
void warp_unit_circle_points_on_xz_plan(std::vector<cv::Vec3f>& points,
                                        float length_of_axis_x,
                                        float length_of_axis_z,
                                        float tx, float tz,
                                        float depth_of_layer_on_y_axis,
                                        std::vector<cv::Vec3f>& output_points);

    
    
void transform_vertices_in_local_coord(std::vector< std::vector<cv::Vec3f> >& points,
                                         float* modelview_mat,
                                         float* projective_mat,
                                         int width,
                                         int height,
                                         float* new_modelview_mat = NULL,
                                         float* new_projective_mat = NULL);

void make_3D_mesh_from_points_cloud(std::vector< std::vector<cv::Vec3f> >& points,
                                    MyMesh &m,
                                    const char* model_path_file);

// To-Do : Need to document.
void get_roi_of_mask_image_for_unity(unsigned char* mask_for_unity, int width, int height,
                               int* rect_x, int* rect_y, int* rect_width, int* rect_height);

    
bool get_head_silhouette_in_image(cv::Mat& webtexture_image, cv::CascadeClassifier& face_cascade, cv::Mat& fgmask);

bool get_silhouette_in_image(cv::Mat& webtexture_image, cv::Rect rect, cv::Mat& fgmask);

void allign_right_mask_on_front_mask( cv::Mat front_mask, cv::Mat right_mask, cv::Mat& new_right_mask, cv::Point& translate);

#ifdef __cplusplus
    }
#endif

#endif /* head_scanner_h */
