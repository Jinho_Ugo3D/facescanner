//
//  main.cpp
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 3. 3..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//

#include <iostream>
#include <string>
#include "face_scanner.h"
#include "gtest/gtest.h"



GTEST_API_ int main(int argc, char **argv) {
    printf("Running main() from gtest_main.cc\n");
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
