//
//  unity_util.cpp
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 5. 12..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//

#include "unity_util.h"


cv::Mat convert_texture2D_to_mat(unsigned char* image_from_unity, int width, int height)
{
    // Convert image in RGB format and left-top origin.
    cv::Mat texture2D = cv::Mat(height, width, CV_8UC4, image_from_unity, cv::Mat::AUTO_STEP);
    cv::Mat rgb_frame = cv::Mat(texture2D.cols, texture2D.rows, CV_8UC3);
    cv::cvtColor(texture2D, rgb_frame, cv::COLOR_BGRA2RGB);
    cv::flip(rgb_frame, rgb_frame, 0);
    
    return rgb_frame.clone();
}

