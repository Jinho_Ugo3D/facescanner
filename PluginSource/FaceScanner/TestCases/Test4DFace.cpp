#include <iostream>
#include <string>


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/tracking.hpp>

#include "face_scanner.h"
#include "head_scanner.h"
#include "video_tracker.h"
#include "unity_util.h"
#include "gtest/gtest.h"


// #define _VIDEO_TEST


// Basic 4D face test fixtures.
namespace {

    
    void dump_mask( cv::Mat mask_for_unity, std::string file )
    {
        cv::Mat temp = mask_for_unity.clone();
        cv::flip(temp, temp, 0);
        cv::threshold(temp, temp, 1.0, 255.0, CV_THRESH_BINARY);
        cv::imwrite(file, temp);
        
    }
    
    
    class Test4DFace: public testing::Test
    {
        
    protected:
        std::string test_data_path;
        std::string test_current_path;
        std::string test_face_scanner_data_path;
        
    protected:
        Test4DFace()
        {
            // Setting current data path.
            char cwd[1024];
            getcwd(cwd, sizeof(cwd));
            test_current_path = std::string(cwd);
            test_face_scanner_data_path = test_current_path+std::string("/Data/");
            test_data_path = test_current_path+std::string("/FaceScanner/TestData");
        }
        
        virtual void SetUp()
        {
            
            // Initialize face scanner.
            std::string model_file = test_face_scanner_data_path + "/"+"sfm_shape_3448.bin";
            std::string mappings_file = test_face_scanner_data_path + "/" + "ibug2did.txt";
            std::string contour_file = test_face_scanner_data_path + "/" + "model_contours.json";
            std::string landmarkdetector_file = test_face_scanner_data_path  + "/" + "face_landmarks_model_rcr_68.bin";
            std::string facedetector_file = test_face_scanner_data_path + "/" + "haarcascade_frontalface_alt2.xml";
            std::string blendshapes_file = test_face_scanner_data_path + "/" + "expression_blendshapes_3448.bin";
            std::string edgetopology_file = test_face_scanner_data_path + "/" + "sfm_3448_edge_topology.json";
            std::string normals_file = test_face_scanner_data_path + "/" + "model_normals.obj";
            std::string profile_facedetector_file = test_face_scanner_data_path + "/" + "haarcascade_profileface.xml";
            
            int result = init_face_scanner(model_file.c_str(), mappings_file.c_str(),
                                           contour_file.c_str(), landmarkdetector_file.c_str(),
                                           facedetector_file.c_str(), profile_facedetector_file.c_str(),
                                           blendshapes_file.c_str(), edgetopology_file.c_str(),
                                           normals_file.c_str() );
            
            EXPECT_EQ(result, EXIT_SUCCESS);
        }
        
        virtual void TearDown()
        {
            clean_up_face_scanner();
        }
        
        cv::Mat LoadImageLikeUnity3DWebTexture( const std::string& filename )
        {
            cv::Mat image = cv::imread(filename, -1); //Read image with alpha channel.
            
            if ( 4 == image.channels() )
            {
                cv::cvtColor(image, image, cv::COLOR_RGBA2BGRA); //Covert BGRA format like OpenGL texture format.
                cv::flip(image, image, 0); //Flip image to emulate OpenGL's origin. (Left-Bottom)
                return image;
            }else if( 3 == image.channels() )
            {
                cv::Mat newImage = cv::Mat( image.rows, image.cols, CV_8UC4 );
                cv::cvtColor(image, newImage, cv::COLOR_RGB2BGRA ); //Covert BGRA format like OpenGL texture format.
                cv::flip(newImage, newImage, 0); //Flip image to emulate OpenGL's origin. (Left-Bottom)
                return newImage;
            }
            
            return image;
        }
        
        void CompareImage( const cv::Mat& src, const cv::Mat& dst)
        {
            cv::Mat diff = (src != dst);
            double sum = cv::sum(diff)[0];
            EXPECT_LE(sum , double(diff.cols*diff.rows)*0.3);

        }

        
        void CompareImages(const std::string& filename1, const std::string& filename2)
        {
            cv::Mat src = cv::imread(filename1, CV_LOAD_IMAGE_COLOR);
            cv::Mat dst = cv::imread(filename2, CV_LOAD_IMAGE_COLOR);
            
            CompareImage(src, dst);
        }
        
        
        void draw_landmark_on_img(cv::Mat img_unity, float* landmark, int number_of_landmark)
        {
            cv::flip(img_unity, img_unity, 0);
            cv::cvtColor(img_unity, img_unity, CV_BGRA2RGBA);
            
            for(int i=0; i<number_of_landmark; ++i)
            {
                cv::circle(img_unity,
                           cv::Point(landmark[2*i],landmark[2*i+1]),
                           5,
                           cv::Scalar(255, 0, 0),
                           -1);
            }
            
        }
        
        cv::Mat FindFaceInTheImage( cv::Mat webcamTextureImage )
        {
            // Try to find facial landmark.
            bool res = find_face_landmark(webcamTextureImage.ptr(), webcamTextureImage.cols, webcamTextureImage.rows);
            EXPECT_TRUE(res);
            
            // Get landamark points.
            float points[68*2];
            int length_of_points = 0;
            res = get_face_landmark_location(points, &length_of_points);
            EXPECT_TRUE(res);
            
            // Prepare the result.
            cv::Mat flip_image;
            cv::flip(webcamTextureImage, flip_image, 0);
            draw_landmark_on_img(flip_image, points, length_of_points);
            cv::cvtColor(flip_image, flip_image, cv::COLOR_BGRA2RGBA);
            
            return flip_image.clone();
        }

        
    };
    

    TEST_F(Test4DFace, DoBasic4DProcessWithOneImage)
    {
        
        // Read test image.
        std::string image_file = test_data_path + std::string("/DoBasic4DProcessWithOneImage/") + std::string("child.png");
        cv::Mat image = LoadImageLikeUnity3DWebTexture(image_file);
        
        // Do 4D Face operation.
        bool res = detect_and_track_face(image.ptr(), image.cols, image.rows);
        EXPECT_TRUE(res);
        
        // Store reconstructed data.
        std::string data_path = test_data_path + std::string("/DoBasic4DProcessWithOneImage/");
        res = capture_mesh_and_texture( data_path.c_str(), "output");
        EXPECT_TRUE(res);
        
        // Get modelview matrix.
        float modelview[16];
        res = get_modelview_mat(modelview);
        EXPECT_TRUE(res);
        std::cout << modelview[0] <<"," << modelview[4]<<"," << modelview[8]<<"," << modelview[12] << std::endl;
        std::cout << modelview[1] <<"," << modelview[5]<<"," << modelview[9]<<"," << modelview[13] << std::endl;
        std::cout << modelview[2] <<"," << modelview[6]<<"," << modelview[10]<<"," << modelview[14] << std::endl;
        std::cout << modelview[3] <<"," << modelview[7]<<"," << modelview[11]<<"," << modelview[15] << std::endl;

        // Get projective matrix.
        float projective[16];
        res = get_projective_mat(projective);
        EXPECT_TRUE(res);
        std::cout << projective[0] <<"," << projective[4]<<"," << projective[8]<<"," << projective[12] << std::endl;
        std::cout << projective[1] <<"," << projective[5]<<"," << projective[9]<<"," << projective[13] << std::endl;
        std::cout << projective[2] <<"," << projective[6]<<"," << projective[10]<<"," << projective[14] << std::endl;
        std::cout << projective[3] <<"," << projective[7]<<"," << projective[11]<<"," << projective[15] << std::endl;

        
        // Compare data.
        CompareImages( data_path+std::string("output.isomap.png"), data_path+std::string("ugo3d.isomap.png")  );
        
        // Get iso map texture.
        int width, height;
        unsigned char* buffer = get_current_iso_map_texture(&width, &height);
        cv::Mat isomap( cv::Size(width,height), CV_8UC4, buffer, cv::Mat::AUTO_STEP);
        cv::imwrite(data_path+std::string("iso_map.png"), isomap);
        
        // Compare data.
        CompareImages( data_path+std::string("expected_iso_map.png"), data_path+std::string("iso_map.png")  );
        
        
        // Get the facial landmark vertices.
        float landmarks_postions_on_face[68*2];
        int length_of_locatoins = 0;
        res = get_face_landmark_location(landmarks_postions_on_face, &length_of_locatoins);
        EXPECT_EQ(length_of_locatoins, 68);
        EXPECT_TRUE(res);
        
        
        // Get texure coord.
        float uv_0[2], uv_1[2];
        res = get_uv_of_vertex_for_unity(225, &uv_0[0], &uv_0[1]);
        EXPECT_TRUE(res);
        
        res = get_uv_of_vertex_for_unity(2797, &uv_1[0], &uv_1[1]);
        EXPECT_TRUE(res);
        
        
    }
    
    TEST_F(Test4DFace, FaceLandmarkDetectTest)
    {
        std::string testcase_data_path = test_data_path + "/"+ "FaceLandmarkDetectTest";
        
        // Read test image.
        std::string image_file = testcase_data_path + "/" + "face_test.png";
        cv::Mat image = LoadImageLikeUnity3DWebTexture(image_file);
        
        // Find face landmark and put it on the image.
        cv::Mat test1;
        cv::Mat ground_truth = cv::imread(testcase_data_path + "/" + "output.png", CV_LOAD_IMAGE_UNCHANGED);
        
        //for( int i=0; i<50; ++i){
            test1 = FindFaceInTheImage(image);
            CompareImage(ground_truth, test1);
        //}
        
        
        // Dump image.
        //std::string dump_image_file = testcase_data_path + "/" + "dump_img.png";
        //cv::imwrite(dump_image_file.c_str(), test1);
        
    
    }
    
    TEST_F(Test4DFace, FaceLandmarkDetectSequenclyTest)
    {
        std::string testcase_data_path = test_data_path + "/"+ "FaceLandmarkDetectTest";
        
        // Try to find face sequentially.
        cv::Mat frame0 = LoadImageLikeUnity3DWebTexture(testcase_data_path + "/" + "frame0.png");
        cv::Mat frame1 = LoadImageLikeUnity3DWebTexture(testcase_data_path + "/" + "frame1.png");
        cv::Mat frame2 = LoadImageLikeUnity3DWebTexture(testcase_data_path + "/" + "frame2.png");
        
        cv::Mat result0 = FindFaceInTheImage(frame0);
        cv::Mat result1 = FindFaceInTheImage(frame1);
        cv::Mat result2 = FindFaceInTheImage(frame2);
        
        // DUMP
        /*
        cv::imwrite(testcase_data_path + "/" + "result_frame0.png", result0);
        cv::imwrite(testcase_data_path + "/" + "result_frame1.png", result1);
        cv::imwrite(testcase_data_path + "/" + "result_frame2.png", result2);
        */
    }

    
// #define DUMP_LANDMARK
    
    TEST_F(Test4DFace, QuickFaceLandmarkTest)
    {
        std::string testcase_data_path = test_data_path + "/"+ "FaceLandmarkDetectTest";
        
        cv::Mat frame0 = LoadImageLikeUnity3DWebTexture(testcase_data_path + "/" + "frame0.png");
        cv::Mat frame1 = LoadImageLikeUnity3DWebTexture(testcase_data_path + "/" + "frame1.png");
        cv::Mat frame2 = LoadImageLikeUnity3DWebTexture(testcase_data_path + "/" + "frame2.png");

        cv::Size img_size = frame0.size();
        
        float gt_landmarks[3][68*2];
        float new_landmarks[3][68*2];

        int old_number_of_landmark = 0;
        int new_number_of_landmark = 0;
        
        cv::Mat temp_img;
        
        // Use find_face_landmark() to get ground truth.
        
        if( find_face_landmark(frame0.ptr(),
                               img_size.width,
                               img_size.height) )
        {
            get_face_landmark_location(gt_landmarks[0], &old_number_of_landmark);

#ifdef DUMP_LANDMARK
            temp_img = frame0.clone();
            draw_landmark_on_img(temp_img, gt_landmarks[0], old_number_of_landmark);
            cv::imwrite(testcase_data_path+"/"+"fl_frame0.png", temp_img);
#endif
            
        }
        
        
        if( find_face_landmark(frame1.ptr(),
                               img_size.width,
                               img_size.height) )
        {
            get_face_landmark_location(gt_landmarks[1], &old_number_of_landmark);

#ifdef DUMP_LANDMARK
            temp_img = frame1.clone();
            draw_landmark_on_img(temp_img, gt_landmarks[1], old_number_of_landmark);
            cv::imwrite(testcase_data_path+"/"+"fl_frame1.png", temp_img);
#endif

        }

        
        if( find_face_landmark(frame2.ptr(),
                               img_size.width,
                               img_size.height) )
        {
            get_face_landmark_location(gt_landmarks[2], &old_number_of_landmark);

#ifdef DUMP_LANDMARK
            temp_img = frame2.clone();
            draw_landmark_on_img(temp_img, gt_landmarks[2], old_number_of_landmark);
            cv::imwrite(testcase_data_path+"/"+"fl_frame2.png", temp_img);
#endif
        }

        
        
        // Use quick_find_face_landmark() to get face landmark quickly.
        if( quick_find_face_landmark(NULL,
                                     frame0.ptr(),
                                     img_size.width,
                                     img_size.height) )
        {
            get_face_landmark_location(new_landmarks[0], &new_number_of_landmark);
            
#ifdef DUMP_LANDMARK
            temp_img = frame0.clone();
            draw_landmark_on_img(temp_img, new_landmarks[0], new_number_of_landmark);
            cv::imwrite(testcase_data_path+"/"+"quick_fl_frame0.png", temp_img);
#endif
        }
        
        
        if( quick_find_face_landmark(frame0.ptr(),
                                     frame1.ptr(),
                                     img_size.width,
                                     img_size.height) )
        {
            get_face_landmark_location(new_landmarks[1], &new_number_of_landmark);
            
#ifdef DUMP_LANDMARK
            temp_img = frame1.clone();
            draw_landmark_on_img(temp_img, new_landmarks[1], new_number_of_landmark);
            cv::imwrite(testcase_data_path+"/"+"quick_fl_frame1.png", temp_img);
#endif

        }
        
        
        if( quick_find_face_landmark(frame1.ptr(),
                                     frame2.ptr(),
                                     img_size.width,
                                     img_size.height) )
        {
            get_face_landmark_location(new_landmarks[2], &new_number_of_landmark);
            
#ifdef DUMP_LANDMARK
            temp_img = frame2.clone();
            draw_landmark_on_img(temp_img, new_landmarks[2], new_number_of_landmark);
            cv::imwrite(testcase_data_path+"/"+"quick_fl_frame2.png", temp_img);
#endif

        }
        
        
        EXPECT_EQ(new_number_of_landmark, old_number_of_landmark);
        
        // Compare and measure the error.
        float acc_err = 0.0;
        for(int i=0; i<3; ++i)
        {
            for(int j=0; j<new_number_of_landmark*2; ++j)
            {
                float err = gt_landmarks[i][j] - new_landmarks[i][j];
                err = 0.5*sqrt(err*err);
                acc_err += err;
            }
        }
        
        EXPECT_LE(acc_err, 500.0);
    }
    
    TEST_F(Test4DFace, FaceLandmarkDetectWithoutFace)
    {
        
        // Read test image without face.
        std::string image_file_without_face_path = test_data_path + std::string("/FaceLandmarkDetectTest/") + std::string("landscape.png");
        cv::Mat image_file_without_face = LoadImageLikeUnity3DWebTexture(image_file_without_face_path);
        
        // Try to find facial landmark.
        bool res = find_face_landmark(image_file_without_face.ptr(), image_file_without_face.cols, image_file_without_face.rows);
        EXPECT_FALSE(res);
    }
    

    TEST_F(Test4DFace, DoBasic4DProcessWithOneImageAndClear)
    {
        
        // Read test image.
        std::string image_file = test_data_path + std::string("/DoBasic4DProcessWithOneImage/") + std::string("child.png");
        cv::Mat image = LoadImageLikeUnity3DWebTexture(image_file);
        
        // Do 4D Face operation.
        bool res = detect_and_track_face(image.ptr(), image.cols, image.rows);
        EXPECT_TRUE(res);
        
        // Clean up face scanner.
        clean_up_face_scanner();
        
        // Do 4D Face operation again.
        res = detect_and_track_face(image.ptr(), image.cols, image.rows);
        EXPECT_FALSE(res);
    }

    TEST_F(Test4DFace, GetHeadSilhouetteInFrontImage)
    {
        
        // Read test image.
        std::string image_file = test_data_path + std::string("/HeadScanner/") + std::string("front_face.jpg");
        cv::Mat webtexture_image = LoadImageLikeUnity3DWebTexture(image_file);
        
        //Face detector initialization.
        std::string facedetector_file = test_face_scanner_data_path + std::string("haarcascade_frontalface_alt2.xml");
        cv::CascadeClassifier face_cascade;
        face_cascade.load(facedetector_file);
        
        // Make silhouette image by detecting the front face.
        bool res = false;
        cv::Mat fg_mask_img = cv::Mat(webtexture_image.cols, webtexture_image.rows, CV_8UC1);
        res = get_head_silhouette_in_image(webtexture_image, face_cascade, fg_mask_img);
        EXPECT_TRUE(res);
        
        // Compare the result.
        std::string ground_truth_file = test_data_path + std::string("/HeadScanner/") + std::string("front_face_silhouette.png");
        cv::Mat ground_truth = cv::imread(ground_truth_file, 0);
        CompareImage(ground_truth, fg_mask_img);
    }
    
    TEST_F(Test4DFace, BuildHeadVolumeWith1Image)
    {
        
        // Read test image.
        std::string image_file = test_data_path + std::string("/HeadScanner/") + std::string("front_face.jpg");
        cv::Mat webtexture_image = LoadImageLikeUnity3DWebTexture(image_file);
        
        // Do 4D Face operation.
        bool res = detect_and_track_face(webtexture_image.ptr(), webtexture_image.cols, webtexture_image.rows);
        EXPECT_TRUE(res);
        
        // Get the projective and modelview parameter.
        float modelview[16];
        res = get_modelview_mat(modelview);
        EXPECT_TRUE(res);
        
        // Get projective matrix.
        float projective[16];
        res = get_projective_mat(projective);
        EXPECT_TRUE(res);

        //Face detector initialization.
        std::string facedetector_file = test_face_scanner_data_path + std::string("haarcascade_frontalface_alt2.xml");
        cv::CascadeClassifier face_cascade;
        face_cascade.load(facedetector_file);
        
        // Make silhouette image by detecting the front face.
        cv::Mat fg_mask_img = cv::Mat(webtexture_image.size(), CV_8UC1);
        res = get_head_silhouette_in_image(webtexture_image, face_cascade, fg_mask_img);
               
    }

    TEST_F(Test4DFace, UnitCircleTest)
    {
        
        std::vector<cv::Vec3f> points_on_unit_circle;
        
        // Generate unit circle.
        int step = 20;
        get_unit_circle_points_on_xz_plan(step, points_on_unit_circle);
        
        EXPECT_EQ(points_on_unit_circle.size(), step);

        // Warp unit circle by setting long-axis and short-axis.
        std::vector<cv::Vec3f> warped_points_on_unit_circle;
        warp_unit_circle_points_on_xz_plan(points_on_unit_circle,
                                           4.5, 10.0, 1.0, 1.0,
                                           0.0,
                                           warped_points_on_unit_circle);
        EXPECT_EQ(warped_points_on_unit_circle.size(), step);
        
    }
    
    
    TEST_F(Test4DFace, ContourModelingTest)
    {
       
        // Read test images.
        std::string testcase_data_path = test_data_path+"/"+"ContourModelingTest";
        cv::Mat front_mask = cv::imread(testcase_data_path+"/"+"real_front_face_mask.png", 0);
        cv::Mat left_mask  = cv::imread(testcase_data_path+"/"+"real_right_face_mask.png",  0);
        
        // Build point cloud.
        std::vector< std::vector<cv::Vec3f> > points_data;
        bulld_3d_points_from_two_blob(front_mask, left_mask, points_data, 100, 30);

        // Build mesh data structure and store it as OBJ file.
        std::string model_file = testcase_data_path+"/"+"dump_model.obj";
        MyMesh m;
        make_3D_mesh_from_points_cloud(points_data, m, model_file.c_str() );
        
        
    }
    
    TEST_F(Test4DFace, ContourModelingTest2)
    {
        
        // Read test images.
        std::string testcase_data_path = test_data_path+"/"+"ContourModelingTest";
        cv::Mat front_mask = cv::imread(testcase_data_path+"/"+"daehee_front_mask.png", 0);
        cv::Mat left_mask  = cv::imread(testcase_data_path+"/"+"daehee_right_mask.png",  0);
        
        // Build point cloud.
        std::vector< std::vector<cv::Vec3f> > points_data;
        bulld_3d_points_from_two_blob(front_mask, left_mask, points_data, 50, 30);
        
        // Build mesh data structure and store it as OBJ file.
        std::string model_file = testcase_data_path+"/"+"dump_model.obj";
        MyMesh m;
        make_3D_mesh_from_points_cloud(points_data, m, model_file.c_str() );
        
        
    }
    
    TEST_F(Test4DFace, ContourModelingBrokenMaskTest)
    {
        
        // Read test images.
        std::string testcase_data_path = test_data_path+"/"+"ContourModelingTest";
        cv::Mat front_mask = cv::imread(testcase_data_path+"/"+"broken_front_face.png", 0);
        cv::Mat right_mask  = cv::imread(testcase_data_path+"/"+"broken_right_face.png",  0);
        
        
        // Build point cloud.
        std::vector< std::vector<cv::Vec3f> > points_data;
        bool ret = bulld_3d_points_from_two_blob(front_mask, right_mask, points_data, 50, 30);
        EXPECT_FALSE(ret);
        
    }
    
    TEST_F(Test4DFace, ContourModelingMaskTest2)
    {
        
        // Read test images.
        std::string testcase_data_path = test_data_path+"/"+"ContourModelingTest2";
        cv::Mat front_face_mask = cv::imread(testcase_data_path+"/"+"Front_Mask.png", 0);
        cv::Mat right_face_mask  = cv::imread(testcase_data_path+"/"+"Right_Mask.png",  0);
        
        // Flip to emulate Unity Texture2D coordinate
        cv::flip(front_face_mask, front_face_mask, 0);
        cv::flip(right_face_mask, right_face_mask, 0);
        
        // Build volume.
        std::string model_file = testcase_data_path+"/"+"dump_model.obj";
        build_head_volume(front_face_mask.ptr(),
                          right_face_mask.ptr(),
                          right_face_mask.size().width,
                          right_face_mask.size().height,
                          NULL,
                          NULL,
                          model_file.c_str(),
                          41,
                          61
                          );
    }
    
    TEST_F(Test4DFace, ContourModelingMaskTest3)
    {
        
        // Read test images.
        std::string testcase_data_path = test_data_path+"/"+"ContourModelingTest3";
        cv::Mat front_face_mask = cv::imread(testcase_data_path+"/"+"Front_Mask.png", 0);
        cv::Mat right_face_mask  = cv::imread(testcase_data_path+"/"+"Left_Mask.png",  0);
        
        // Flip to emulate Unity Texture2D coordinate
        cv::flip(front_face_mask, front_face_mask, 0);
        cv::flip(right_face_mask, right_face_mask, 0);
        
        // Build volume.
        std::string model_file = testcase_data_path+"/"+"dump_model.obj";
        build_head_volume(front_face_mask.ptr(),
                          right_face_mask.ptr(),
                          right_face_mask.size().width,
                          right_face_mask.size().height,
                          NULL,
                          NULL,
                          model_file.c_str(),
                          41,
                          61
                          );
    }

    // Will implement later.
    TEST_F(Test4DFace, GrabCutMaskTest)
    {
        int iter = 7;
        
        
        // Read the data.
        std::string testcase_data_path = test_data_path+"/"+"GrabCutMaskTest";
        cv::Mat webtexture_face_image = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"front_face3.png");
        cv::Size image_size = webtexture_face_image.size();

        if( find_face_landmark(webtexture_face_image.ptr(),
                           image_size.width,
                           image_size.height) )
        {
            float landmarks[200];
            int length_of_landmarks;

            if( get_face_landmark_location(landmarks, &length_of_landmarks) )
            {
             
                cv::Mat mask_for_unity( webtexture_face_image.size(), CV_8U );
                get_mask_from_face_landmark(landmarks, length_of_landmarks,
                                            image_size.width, image_size.height,
                                            mask_for_unity.ptr()
                                            );

                dump_mask(mask_for_unity, testcase_data_path+"/"+ "init_mask.png" );

                
                refine_mask_by_grabcut(webtexture_face_image.ptr(),
                                       image_size.width, image_size.height, iter,
                                       mask_for_unity.ptr() );
                
                dump_mask(mask_for_unity, testcase_data_path+"/"+ "mask.png" );
                
                
                int rect[4];
                get_roi_of_mask_image_for_unity(mask_for_unity.ptr(),
                                                mask_for_unity.cols, mask_for_unity.rows,
                                                &rect[0], &rect[1], &rect[2], &rect[3]);
                std::cout<< rect[0]<<", "<< rect[1]<<", "<< rect[2]<<", "<< rect[3]<<", "<< std::endl;

                
                // Put user's stroke for foreground and background.
                const int GC_USER_FGD=4;
                const int GC_USER_BGD=5;
                cv::line(mask_for_unity,
                         cv::Point(100,100), cv::Point(750,431),
                         cv::Scalar( GC_USER_BGD ), 50 );
                
                cv::line(mask_for_unity,
                         cv::Point(200,400), cv::Point(631,823),
                         cv::Scalar( GC_USER_FGD), 70 );
                
                dump_mask(mask_for_unity, testcase_data_path+"/"+ "mask_interaction.png" );
                
                refine_mask_by_grabcut(webtexture_face_image.ptr(),
                                       image_size.width, image_size.height, iter,
                                       mask_for_unity.ptr(), true );

                dump_mask(mask_for_unity, testcase_data_path+"/"+ "mask_interaction2.png" );
                
                


            }

        }

    }
    
    
    TEST_F(Test4DFace, FaceReconstuctionTest)
    {
        // Read the data.
        std::string testcase_data_path = test_data_path+"/"+"FaceReconstructionTest";
        cv::Mat webtexture_front_face_image = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"front_face.png");
        cv::Mat webtexture_right_face_image = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"right_face.png");

        // Do 4D Face operation for front face..
        bool res = detect_and_track_face(webtexture_front_face_image.ptr(),
                                         webtexture_front_face_image.cols,
                                         webtexture_front_face_image.rows);
        EXPECT_TRUE(res);
        
        // Store reconstructed data.
        res = capture_mesh_and_texture(testcase_data_path.c_str(), "output");
        EXPECT_TRUE(res);
        
        // Get modelview matrix.
        float modelview[16];
        res = get_modelview_mat(modelview);
        EXPECT_TRUE(res);
        
        // Get projective matrix.
        float projective[16];
        res = get_projective_mat(projective);
        EXPECT_TRUE(res);
        
        // Get the mask of front face image.
        cv::Size image_size = webtexture_front_face_image.size();
        cv::Mat front_face_mask(image_size, CV_8UC1);
        
        get_ellipse_mask(410, image_size.height-550, 310, 360,
                         image_size.width, image_size.height,
                         front_face_mask.ptr()
                         );

        
        refine_mask_by_grabcut(webtexture_front_face_image.ptr(),
                                webtexture_front_face_image.cols,
                                webtexture_front_face_image.rows,
                                5,
                                front_face_mask.ptr()
                                );
        
        // Get the mask of right face image.
        cv::Mat right_face_mask(image_size, CV_8UC1);
        get_ellipse_mask(webtexture_right_face_image.cols/2,
                         webtexture_right_face_image.rows/2,
                         310, 360,
                         image_size.width, image_size.height,
                         right_face_mask.ptr()
                         );
        
        refine_mask_by_grabcut(webtexture_right_face_image.ptr(),
                                webtexture_right_face_image.cols,
                                webtexture_front_face_image.rows,
                                5,
                                right_face_mask.ptr()
                                );
        
        
        // DUMP blobs.
        cv::imwrite(testcase_data_path+"/"+"dump_front_face_mask.png", front_face_mask);
        cv::imwrite(testcase_data_path+"/"+"dump_right_face_mask.png", right_face_mask);
        
        
        // Build volume.
        std::string model_file = testcase_data_path+"/"+"dump_model.obj";
        build_head_volume(front_face_mask.ptr(),
                          right_face_mask.ptr(),
                          image_size.width,
                          image_size.height,
                          modelview,
                          projective,
                          model_file.c_str(),
                          41,
                          61
                          );
        
    }
    
    TEST_F(Test4DFace, FaceReconstuctionTest2)
    {
        // Read the data.
        std::string testcase_data_path = test_data_path+"/"+"FaceReconstructionTest2";
        cv::Mat webtexture_front_face_image = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"front_face.png");
        cv::Mat webtexture_right_face_image = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"right_face.png");
        
        // Do 4D Face operation for front face..
        bool res = detect_and_track_face(webtexture_front_face_image.ptr(),
                                         webtexture_front_face_image.cols,
                                         webtexture_front_face_image.rows);
        EXPECT_TRUE(res);
        
        // Store reconstructed data.
        res = capture_mesh_and_texture(testcase_data_path.c_str(), "output");
        EXPECT_TRUE(res);
        
        // Get modelview matrix.
        float modelview[16];
        res = get_modelview_mat(modelview);
        EXPECT_TRUE(res);
        
        // Get projective matrix.
        float projective[16];
        res = get_projective_mat(projective);
        EXPECT_TRUE(res);
        
        // Get the mask of front face image.
        cv::Size image_size = webtexture_front_face_image.size();
        cv::Mat front_face_mask(image_size, CV_8UC1);
        
        get_ellipse_mask(222, image_size.height-284, 347/2, 442/2,
                         image_size.width, image_size.height,
                         front_face_mask.ptr()
                         );
        
        
        refine_mask_by_grabcut(webtexture_front_face_image.ptr(),
                               webtexture_front_face_image.cols,
                               webtexture_front_face_image.rows,
                               12,
                               front_face_mask.ptr()
                               );
        
        // Get the mask of right face image.
        cv::Mat right_face_mask(image_size, CV_8UC1);
        get_ellipse_mask(171, image_size.height-303, 347/2, 442/2,
                         image_size.width, image_size.height,
                         right_face_mask.ptr()
                         );
        
        refine_mask_by_grabcut(webtexture_right_face_image.ptr(),
                               webtexture_right_face_image.cols,
                               webtexture_front_face_image.rows,
                               12,
                               right_face_mask.ptr()
                               );
        
        
        // DUMP blobs.
        cv::imwrite(testcase_data_path+"/"+"dump_front_face_mask.png", front_face_mask);
        cv::imwrite(testcase_data_path+"/"+"dump_right_face_mask.png", right_face_mask);
        
        
        // Build volume.
        std::string model_file = testcase_data_path+"/"+"dump_model.obj";
        build_head_volume(front_face_mask.ptr(),
                          right_face_mask.ptr(),
                          image_size.width,
                          image_size.height,
                          modelview,
                          projective,
                          model_file.c_str(),
                          81,
                          61
                          );
        
    }
    
    
    
   
  
    
    TEST_F(Test4DFace, SideMaskTest)
    {
        
        std::string testcase_data_path = test_data_path+"/"+"SideMaskTest"+"/"+"case5";
        cv::Mat webtexture_front_face_image = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"front.png");
        cv::Mat webtexture_right_face_image = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"right.png");
        cv::Mat webtexture_left_face_image  = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"left.png" );
        
        
        const int iter = 7;
        
        // Do 4D Face operation for front face..
        bool res = detect_and_track_face(webtexture_front_face_image.ptr(),
                                         webtexture_front_face_image.cols,
                                         webtexture_front_face_image.rows);
        EXPECT_TRUE(res);
        
        
        // Get the mask of front face image.
        cv::Size image_size = webtexture_front_face_image.size();
        cv::Mat front_face_mask(image_size, CV_8UC1);
        
        
        // Init front face mask by face landmark.
        float landmark_points[200];
        int length_of_landmark_points = 0;
        res = find_face_landmark(webtexture_front_face_image.ptr(), image_size.width, image_size.height);
        EXPECT_TRUE(res);
        get_face_landmark_location(landmark_points, &length_of_landmark_points);
        get_mask_from_face_landmark(landmark_points, length_of_landmark_points,
                                    image_size.width, image_size.height,
                                    front_face_mask.ptr() );
        
        
        
        // DUMP init mask for front face.
        dump_mask(front_face_mask, testcase_data_path+"/"+"dump_init_front_face_mask.png");
        
        
        refine_mask_by_grabcut(webtexture_front_face_image.ptr(),
                               webtexture_front_face_image.cols,
                               webtexture_front_face_image.rows,
                               iter,
                               front_face_mask.ptr()
                               );
        
        // DUMP masks.
        dump_mask(front_face_mask, testcase_data_path+"/"+"dump_front_face_mask.png" );
        front_face_mask.release();
        
        
        // Get the mask of left face image.
        cv::Mat left_face_mask(image_size, CV_8UC1);
        get_side_mask_from_face_landmark(landmark_points, length_of_landmark_points,
                                         image_size.width, image_size.height,
                                         true,
                                         left_face_mask.ptr() );
        
        
        // DUMP init mask for left face.
        dump_mask(left_face_mask, testcase_data_path+"/"+"dump_init_left_face_mask.png");
        
        refine_mask_by_grabcut(webtexture_left_face_image.ptr(),
                               image_size.width,
                               image_size.height,
                               iter,
                               left_face_mask.ptr()
                               );
        
        // DUMP masks.
        dump_mask(left_face_mask,  testcase_data_path+"/"+"dump_left_face_mask.png"  );
        left_face_mask.release();
        
        
        // Get the mask of right face image.
        cv::Mat right_face_mask(image_size, CV_8UC1);
        get_side_mask_from_face_landmark(landmark_points, length_of_landmark_points,
                                         image_size.width, image_size.height,
                                         false,
                                         right_face_mask.ptr() );
        
        
        // DUMP init mask for right face.
        dump_mask(right_face_mask, testcase_data_path+"/"+"dump_init_right_face_mask.png");
        //cv::imwrite(testcase_data_path+"/"+"dump_init_right_face_mask.png", right_face_mask);
        
        
        refine_mask_by_grabcut(webtexture_right_face_image.ptr(),
                               webtexture_right_face_image.cols,
                               webtexture_front_face_image.rows,
                               iter,
                               right_face_mask.ptr()
                               );
        
        // DUMP masks.
        dump_mask(right_face_mask, testcase_data_path+"/"+"dump_right_face_mask.png" );
        right_face_mask.release();
        
        // Release memories
        webtexture_front_face_image.release();
        webtexture_right_face_image.release();
        webtexture_left_face_image.release();

    }
    

    TEST_F(Test4DFace, AlignFrontAndRightFaceMask)
    {
        // Read the data.
        std::string testcase_data_path
            = test_data_path+"/"+"AlignFrontAndRightFaceMask";
        cv::Mat front_face_mask_image
            = cv::imread(testcase_data_path+"/"+"front_mask.png",
                         CV_LOAD_IMAGE_GRAYSCALE);
        cv::Mat right_face_mask_image
            = cv::imread(testcase_data_path+"/"+"right_mask.png",
                         CV_LOAD_IMAGE_GRAYSCALE);
        
        cv::Mat new_right_face_mask;
        cv::Point translate;
        allign_right_mask_on_front_mask(
                                      front_face_mask_image,
                                      right_face_mask_image,
                                      new_right_face_mask,
                                      translate);
        
        // Test the result.
        ASSERT_NE(translate.x, 0);
        ASSERT_NE(translate.y, 0);
        
        cv::Mat control_group
            = cv::imread(testcase_data_path+"/"+"aligned_right_mask.png",
                         CV_LOAD_IMAGE_GRAYSCALE);
        CompareImage(new_right_face_mask, control_group);
        
        
        // Dump
        //cv::imwrite(testcase_data_path+"/new_mask.png", new_right_face_mask);
        
    }
    
    TEST_F(Test4DFace, AlignFrontAndRightFaceMask2)
    {
        // Read the data.
        std::string testcase_data_path
        = test_data_path+"/"+"AlignFrontAndRightFaceMask2";
        cv::Mat front_face_mask_image
        = cv::imread(testcase_data_path+"/"+"front_mask.png",
                     CV_LOAD_IMAGE_GRAYSCALE);
        cv::Mat right_face_mask_image
        = cv::imread(testcase_data_path+"/"+"right_mask.png",
                     CV_LOAD_IMAGE_GRAYSCALE);
        
        cv::Mat new_right_face_mask;
        cv::Point translate;
        allign_right_mask_on_front_mask(
                                        front_face_mask_image,
                                        right_face_mask_image,
                                        new_right_face_mask,
                                        translate);
        
        // Test the result.
        ASSERT_NE(translate.x, 0);
        ASSERT_NE(translate.y, 0);
        
        cv::Mat control_group
            = cv::imread(testcase_data_path+"/"+"aligned_right_mask.png",
                         CV_LOAD_IMAGE_GRAYSCALE);
        CompareImage(new_right_face_mask, control_group);
        
        
        // Dump
//        cv::imwrite(testcase_data_path+"/new_mask.png", new_right_face_mask);
        
    }
    
    
    TEST_F(Test4DFace, EllipseMaskByFaceLandmark)
    {
        // Read the data.
        std::string testcase_data_path = test_data_path+"/"+"EllipseMaskByFaceLandmark";
        std::string image_file = testcase_data_path + "/" + "front_picture.png";
        cv::Mat image = LoadImageLikeUnity3DWebTexture(image_file);
        
        
        // Find face landmark.
        bool res = find_face_landmark(image.ptr(), image.cols, image.rows);
        EXPECT_TRUE(res);
        
        // Get landamark points.
        float points[68*2];
        int length_of_points = 0;
        res = get_face_landmark_location(points, &length_of_points);
        EXPECT_TRUE(res);
        
        // Get mask from landmark points.
        cv::Mat mask_for_unity(image.size(), CV_8UC1);
        get_mask_from_face_landmark(points, 68,
                                    image.cols, image.rows,
                                    mask_for_unity.ptr() );
        
        // Test the result.
        cv::Mat control_group
            = cv::imread(testcase_data_path+"/"+"gt_mask.png",
                     CV_LOAD_IMAGE_GRAYSCALE);
        CompareImage(mask_for_unity, control_group);
        
        
        // Dump
//        cv::Mat flip_image;
//        cv::flip(image, flip_image, 0);
//        cv::cvtColor(flip_image, flip_image, cv::COLOR_BGRA2RGBA);
//        
//        for(int i=0; i<length_of_points; ++i)
//        {
//            cv::circle(flip_image,
//                       cv::Point(points[2*i],points[2*i+1]),
//                       5,
//                       cv::Scalar(255, 0, 0, 255),
//                       -1);
//        }
//        cv::imwrite(testcase_data_path+"/"+"image_with_landmark.png", flip_image);
//        cv::imwrite(testcase_data_path+"/"+"mask.png", mask_for_unity); // Dump mask image as unity texture2d format.

    }
    
 
    
    TEST_F(Test4DFace, Get3DFaceLandmarkTest)
    {
        
        // Read test image.
        std::string testcase_data_path = test_data_path+"/"+"DoBasic4DProcessWithOneImage";
        std::string image_file = testcase_data_path+ "/" + std::string("child.png");
        cv::Mat image = LoadImageLikeUnity3DWebTexture(image_file);
        
        // Do 4D Face operation.
        bool res = detect_and_track_face(image.ptr(), image.cols, image.rows);
        EXPECT_TRUE(res);
        
        // Get the 3D landmark position.
        float landmark_3d_list[68*3];
        int length = -1;
        res = get_face_landmark_in_3D_on_4D_face_mesh(landmark_3d_list, &length);
        EXPECT_TRUE(res);
        EXPECT_GE(length, 0);
        

        // Dump 3D data.
//        std::string threed_file_path = testcase_data_path + "/"+ "3d_landmark.obj";
//        FILE*  pFile = fopen( threed_file_path.c_str(),"w");
//        for ( int i=0; i < length; ++i)
//        {
//            fprintf(pFile, "v %f %f %f\n",
//                    landmark_3d_list[3*i+0],
//                    landmark_3d_list[3*i+1],
//                    landmark_3d_list[3*i+2]
//                    );
//        }
//        fclose(pFile);
        
        
        // Clean up face scanner.
        clean_up_face_scanner();
        
    }
    
    
    TEST_F(Test4DFace, EllipseMaskForSideFace)
    {
        bool res;
        
        // Read the data.
        std::string testcase_data_path = test_data_path+"/"+"EllipseMaskForSideFace";
        
        // Data file array
        std::string files[] = {"face0.png", "face1.png", "face2.png", "face3.png", "face4.png", "face5.png"};
        int length = sizeof(files) / sizeof(files[0]);
        
        // Run test
        for (int i=0; i<length; ++i)
        {
            std::string image_file = files[i];
            
            cv::Mat image = LoadImageLikeUnity3DWebTexture(testcase_data_path + "/" + image_file);
            cv::Mat mask_for_unity(image.size(), CV_8UC1);
            res = get_side_face_mask(image.ptr(),
                                     image.size().width,
                                     image.size().height,
                                     mask_for_unity.ptr() );
            
            if(res)
            {
                cv::Mat ground_truth
                            = cv::imread(testcase_data_path+"/"+"gt_mask_"+image_file, CV_LOAD_IMAGE_GRAYSCALE);
                CompareImage(ground_truth, mask_for_unity);
                ground_truth.release();
                
                // Dump
                //dump_mask(mask_for_unity, testcase_data_path+"/"+"mask_"+image_file );
                //cv::imwrite(testcase_data_path+"/"+"mask_"+image_file, mask_for_unity);
            }
            
            mask_for_unity.release();
            image.release();
        }
    }
    
    
    TEST_F(Test4DFace, FaceRectTest)
    {
        // Read the data.
        std::string testcase_data_path = test_data_path+"/"+"FaceRectTest";

        // Test 1st data.
        std::string file1 = testcase_data_path+"/"+"face2.png";
        cv::Mat current_frame = LoadImageLikeUnity3DWebTexture(file1);
        cv::Size img_size = current_frame.size();

        // Detect face.
        cv::Rect rect;
        bool res = false;
        res = find_face_rect(current_frame.ptr(), img_size.width, img_size.height,
                             &rect.x, &rect.y, &rect.width, &rect.height );
        
        ASSERT_TRUE(res);
        if( res )
        {
            std::cout<< rect << std::endl;
        }
        
        // Test small image. Have to fail.
        cv::Mat current_frame2 = LoadImageLikeUnity3DWebTexture(testcase_data_path+"/"+"face1.png");
        img_size = current_frame2.size();
        res = find_face_rect(current_frame2.ptr(), img_size.width, img_size.height,
                                  &rect.x, &rect.y, &rect.width, &rect.height );
        
        ASSERT_FALSE(res);
        
    }


#ifdef _VIDEO_TEST
    
    TEST_F(Test4DFace, FaceMarkTrackingTest)
    {
        
        // Read the data.
        std::string testcase_data_path = test_data_path+"/"+"FaceMarkTrackingTest";
        std::string data_path = testcase_data_path + "/" + "File_006";
        std::string result_path = testcase_data_path + "/" + "File_006_result";
        int length_of_frames = 230;
        
        
        // Init video trakcer.
        bool res = unity_init_video_tracker(6.0f, 21);
        ASSERT_TRUE(res);

        
        cv::Mat current_frame, prev_frame;
        
        for( int i=0; i< length_of_frames; ++i)
        {
            // Read each video frame.
            std::string file_path = data_path+"/"+"frame"+std::to_string(i)+".png";
            current_frame = LoadImageLikeUnity3DWebTexture(file_path);
            cv::Size img_size = current_frame.size();

            
            // Track key landmark points.
            // Face landmarks to track.
            //
            //   iBug face mark annotation
            //  * 46 ; left eye outer-corner (2)
            //  * 36 ; left nostril, below nose, nose-lip junction
            //  * 55 ; left mouth corner (13)
            //  * 37 ; right eye outer-corner (1)
            //  * 32 ; right nostril, below nose, nose-lip junction
            //  * 49 ; right mouth corner (12)
            float tracked_points[100];
            int number_of_face_landmark = 0;
            
            float reliability = 0.0;
            bool using_optical_flow = false;
            res = unity_track_face_landmark(i, current_frame.ptr(), prev_frame.ptr(),
                                    img_size.width,img_size.height,
                                    tracked_points, &number_of_face_landmark,
                                    &reliability, &using_optical_flow);
            
            
            //Backup current frame in previous frame.
            prev_frame = current_frame.clone();
            
            
            if(!res)
                continue;
            
            // Dump tracked points
            cv::Mat temp_img = current_frame.clone();
            flip(temp_img, temp_img, 0);
            cv::cvtColor(temp_img, temp_img, CV_BGRA2RGB);

            cv::Scalar colors[6] = { cv::Scalar(0, 0, 255), cv::Scalar(0, 255, 0), cv::Scalar(255, 0, 0),
                cv::Scalar(0, 255, 255), cv::Scalar(255, 255, 0), cv::Scalar(255, 0, 255) };

            
            for ( int j=0; j<number_of_face_landmark; ++j)
            {
                cv::circle(temp_img,
                           cv::Point(tracked_points[2*j],tracked_points[2*j+1] ),
                           5,
                           colors[j],
                           -1 );
            }

            cv::imwrite(result_path+"/"+"result_frame_"+std::to_string(i)+".png", temp_img);

            std::cout << "Processed frame: "<< std::to_string(i) << std::endl;
            std::cout << "Reliability: "<< reliability << std::endl;
            std::cout << "Is using optical flow? "<< using_optical_flow << std::endl;

            
            temp_img.release();

        }
        
        res = unity_destroy_video_tracker();
        ASSERT_TRUE(res);
                    
        
    }
    #endif //_VIDEO_TEST
    


    
#ifdef _VIDEO_TEST
    TEST_F(Test4DFace, QuickFaceLandmarkVideoTest)
    {
        // Read the data.
        std::string testcase_data_path = test_data_path+"/"+"FaceMarkTrackingTest";
        std::string data_path = testcase_data_path + "/" + "File_005";
        std::string result_path = testcase_data_path + "/" + "File_005_result";
        int length_of_frames = 321;
        
        cv::Mat current_frame, prev_frame;
        
        float landmarks[200];
        int number_of_landmark = 0;
        
        for( int idx=0; idx< length_of_frames; ++idx)
        {
            
            std::cout << std::to_string(idx)+" frame :" << std::endl;
            
            // Read each video frame.
            std::string file_path = data_path+"/"+"frame"+std::to_string(idx)+".png";
            current_frame = LoadImageLikeUnity3DWebTexture(file_path);
            cv::Size img_size = current_frame.size();
            
            
            // Do quick face landmark.
            bool res = false;
            if( 0 == idx)
                res = quick_find_face_landmark(NULL, current_frame.ptr(), img_size.width, img_size.height);
            else
                res = quick_find_face_landmark(prev_frame.ptr(), current_frame.ptr(), img_size.width, img_size.height);
 
            
            // Get the result.
            if (!res)
                std::cout << "Will reinitlize facial landmark." << std::endl;
            
            // Get the result.
            get_face_landmark_location(landmarks, &number_of_landmark);
            
            
            // Dump the result
            cv::Mat temp_img = current_frame.clone();
            draw_landmark_on_img(temp_img, landmarks, number_of_landmark);
            cv::imwrite(result_path+"/"+"result_frame"+std::to_string(idx)+".png", temp_img);
            temp_img.release();
            
            //Backup current frame in previous frame.
            prev_frame = current_frame.clone();
            
            
        }
        
        
    }
    
#endif //_VIDEO_TEST
    
}






