//
//  video_tracker.cpp
//  FaceScanner
//
//  Created by JinhoYoo on 2017. 5. 29..
//  Copyright © 2017년 JinhoYoo. All rights reserved.
//


#include <iostream>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <numeric>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/video/tracking.hpp>

#include "unity_util.h"
#include "face_scanner.h"
#include "video_tracker.h"

using cv::Mat;
using cv::Vec2f;
using cv::Vec3f;
using cv::Vec4f;
using cv::Scalar;
using cv::Rect;
using std::cout;
using std::endl;
using std::vector;
using std::string;


typedef struct VideoTracker{
    
    //  Face landmark to track.
    //
    //  * 46 ; left eye outer-corner (2)
    //  * 36 ; left nostril, below nose, nose-lip junction
    //  * 55 ; left mouth corner (13)
    //  * 37 ; right eye outer-corner (1)
    //  * 32 ; right nostril, below nose, nose-lip junction
    //  * 49 ; right mouth corner (12)
    std::vector<cv::Point2f> cur_landmark_to_track;
    std::vector<cv::Point2f> prev_landmark_to_track;
    
    // Distance between eye, norstril, and eye measured by tracker in every frame.
    std::vector<float> distance;
    
    
    // Optical flow options
    int window_size = 21;
    float distance_limit = 7.0f;
    
    // Optical flow options
    std::vector<uchar> status;
    std::vector<float> err;
    
    // Use face detector or optical flow?
    bool use_face_detector = true;
    
} VIDEOTRACKER;






bool _track_face_landmark_by_face_detector(unsigned char* image_for_unity,
                                          int width, int height,
                                          std::vector<cv::Point2f>& cur_landmark_to_track)
{
    


    
    if( find_face_landmark(image_for_unity, width, height) )
    {
        
        // Initialize initial landmark.
        float landmark[200];
        int length_of_landmark;
        get_face_landmark_location(landmark, &length_of_landmark);
        
        //  * 46 ; left eye outer-corner (2)
        cur_landmark_to_track[0] = cv::Point2d( landmark[2*45+0], landmark[2*45+1] );
        
        //  * 36 ; left nostril, below nose, nose-lip junction
        cur_landmark_to_track[1] = cv::Point2d( landmark[2*35+0], landmark[2*35+1] );
        
        //  * 55 ; left mouth corner (13)
        cur_landmark_to_track[2] = cv::Point2d( landmark[2*54+0], landmark[2*54+1] );
        
        //  * 37 ; right eye outer-corner (1)
        cur_landmark_to_track[3] = cv::Point2d( landmark[2*36+0], landmark[2*36+1] );
        
        //  * 32 ; right nostril, below nose, nose-lip junction
        cur_landmark_to_track[4] = cv::Point2d( landmark[2*31+0], landmark[2*31+1] );
        
        //  * 49 ; right mouth corner (12)
        cur_landmark_to_track[5] = cv::Point2d( landmark[2*48+0], landmark[2*48+1] );
        
        return true;
        
    }else{
        std::cout <<"No face in the frame!!" << std::endl;
        return false;
    }
    
}

bool _is_reliable_track_for_optical_flow( int frame_index,
                                        std::vector<cv::Point2f>& cur_landmark_to_track,
                                        std::vector<cv::Point2f>& prev_landmark_to_track,
                                        std::vector<float>& distance )
{
    if ( 0 == frame_index )
        return true;
    
    for(int i=0;i<3; ++i)
    {
        float diff =
        cv::norm( cur_landmark_to_track[i] - prev_landmark_to_track[i] );
        
        if( diff > distance[i] )
            return false;
        
    }
    
    return true;
}


bool _is_reliable_track_for_face_tracker( int frame_index,
                                        std::vector<cv::Point2f>& cur_landmark_to_track,
                                        std::vector<cv::Point2f>& prev_landmark_to_track,
                                        float distance_limit )
{
    if ( 0 == frame_index )
        return true;
    
    for(int i=0;i<3; ++i)
    {
        float diff =
        cv::norm( cur_landmark_to_track[i] - prev_landmark_to_track[i] );
        
        if ( diff > distance_limit)
            return false;
    }
    
    return true;
}


void _track_face_landmark_by_optical_flow(unsigned char* cur_img_for_unity,
                                         unsigned char* prev_img_for_unity,
                                         int width, int height,
                                         int window_size,
                                         std::vector<cv::Point2f>& cur_landmark_to_track,
                                         std::vector<cv::Point2f>& prev_landmark_to_track,
                                         std::vector<uchar>& status,
                                         std::vector<float>& err)
{
    
    cv::Size win_size(window_size,window_size);
    cv::TermCriteria termcrit(cv::TermCriteria::COUNT|cv::TermCriteria::EPS,20,0.03);
    
    cv::Mat temp_cur_img = convert_texture2D_to_mat(cur_img_for_unity, width, height);
    cv::Mat temp_prev_img = convert_texture2D_to_mat(prev_img_for_unity, width, height);
    
    cv::Mat gray, prev_gray;
    cvtColor(temp_cur_img, gray, cv::COLOR_BGR2GRAY);
    cvtColor(temp_prev_img, prev_gray, cv::COLOR_BGR2GRAY);
    
    cv::calcOpticalFlowPyrLK(prev_gray, gray,
                             prev_landmark_to_track, cur_landmark_to_track,
                             status, err, win_size,
                             3, termcrit, 0, 0.001);
}





// Unity plugin interface
//====================
VIDEOTRACKER* g_video_tracker = NULL;


bool unity_init_video_tracker(const float distance_limit, const int window_size)
{
    if (NULL == g_video_tracker)
    {
        g_video_tracker = new VideoTracker;
        
        g_video_tracker->cur_landmark_to_track.resize(2*3);
        g_video_tracker->prev_landmark_to_track.resize(2*3);
        g_video_tracker->distance.resize(3);
        
        g_video_tracker->distance_limit = distance_limit;
        g_video_tracker->window_size = window_size;
        
        std::cerr << "Initialized video tracker. " << std::endl;
        return true;
    }else{
        std::cerr << "Fail to initialized video tracker. Already initialized. " << std::endl;
        return false;
    }
}


bool unity_destroy_video_tracker()
{
    
    if( g_video_tracker)
    {
        std::cerr << "Destroy video tracker." << std::endl;
        delete g_video_tracker;
        g_video_tracker = NULL;
        return true;
    }else{
        std::cerr << "Fail to destroy video tracker. Not initialized. " << std::endl;
        return false;
    }
    
}


bool unity_track_face_landmark(int idx,
                               unsigned char* cur_img_for_unity,
                               unsigned char* prev_img_for_unity,
                               int width, int height,
                               float* current_key_face_landmark,
                               int* number_of_face_landmark,
                               float* reliability,
                               bool* using_optical_flow)
{
    bool res = false;
    if( !g_video_tracker)
    {
        std::cerr << "Not initialized video tracker! " << std::endl;
        return false;
    }
    
    float reliablity_value = 0.0;
    if( g_video_tracker->use_face_detector )
    {
        
        res = _track_face_landmark_by_face_detector(
                                            cur_img_for_unity,
                                            width, height,
                                            g_video_tracker->cur_landmark_to_track);
    
        
        if( !res)
        {
            std::cerr << "No face in video frame." << std::endl;
            return false;
        }
        
        // Measure distance among face landmarks in initial frame.
        if( 0 == idx )
        {
            
            for(int j=0;j<3; ++j)
            {
                g_video_tracker->distance[j] =
                    cv::norm( g_video_tracker->cur_landmark_to_track[j]
                             - g_video_tracker->cur_landmark_to_track[j+3] );
            }
            
            g_video_tracker->use_face_detector = false;
        }
        
        if( !_is_reliable_track_for_face_tracker(idx,
                                                 g_video_tracker->cur_landmark_to_track,
                                                 g_video_tracker->prev_landmark_to_track,
                                                 g_video_tracker->distance_limit) )
        {
            g_video_tracker->use_face_detector = false;
            
            _track_face_landmark_by_optical_flow(cur_img_for_unity,
                                                 prev_img_for_unity,
                                                 width, height,
                                                 g_video_tracker->window_size,
                                                 g_video_tracker->cur_landmark_to_track,
                                                 g_video_tracker->prev_landmark_to_track,
                                                 g_video_tracker->status,
                                                 g_video_tracker->err);
            
        }else
            reliablity_value = 1.0;
        
       
        
    }else
    {
        // Track points by opticalflow.
        _track_face_landmark_by_optical_flow(cur_img_for_unity,
                                             prev_img_for_unity,
                                             width, height,
                                             g_video_tracker->window_size,
                                             g_video_tracker->cur_landmark_to_track,
                                             g_video_tracker->prev_landmark_to_track,
                                             g_video_tracker->status,
                                             g_video_tracker->err);
        
        
        
        
        if( _is_reliable_track_for_optical_flow(idx,
                                                g_video_tracker->cur_landmark_to_track,
                                                g_video_tracker->prev_landmark_to_track,
                                                g_video_tracker->distance)
           )
        {
            // keep previous point to track again in next frame.
            // Masure the reliability.
            float succeed_case = 0.0;
            for(int j=0; j<g_video_tracker->cur_landmark_to_track.size(); ++j)
            {
                if( g_video_tracker->status[j] )
                    succeed_case += 1.0;
                
            }
            
            reliablity_value = succeed_case / 6.0;
            
            // If reliability is low, then use face detector to reinitialize face landmark.
            if (reliablity_value < 1.0f)
            {
                
                _track_face_landmark_by_face_detector(cur_img_for_unity,
                                                      width, height,
                                                      g_video_tracker->cur_landmark_to_track);
                 g_video_tracker->use_face_detector = true;
            }
        }else{
            
            _track_face_landmark_by_face_detector(cur_img_for_unity,
                                                  width, height,
                                                  g_video_tracker->cur_landmark_to_track);
            g_video_tracker->use_face_detector = true;
            
        }
    }
    
    // Backup current landmark.
    std::copy(g_video_tracker->cur_landmark_to_track.begin(),
              g_video_tracker->cur_landmark_to_track.end(),
              g_video_tracker->prev_landmark_to_track.begin() );
    
    // Copy current tracked points.
    for( int i=0; i< static_cast<int>(g_video_tracker->cur_landmark_to_track.size()); ++i  )
    {
        current_key_face_landmark[2*i+0] = g_video_tracker->cur_landmark_to_track[i].x;
        current_key_face_landmark[2*i+1] = g_video_tracker->cur_landmark_to_track[i].y;
    }
    *number_of_face_landmark = static_cast<int>( g_video_tracker->cur_landmark_to_track.size() );
    
    // Return the reliability and whether to use optical flow or not.
    if( NULL != reliability)
        *reliability = reliablity_value;
    
    if( NULL != using_optical_flow)
        *using_optical_flow = !(g_video_tracker->use_face_detector);
    
    
    // Show the reliability.
    //std::cout << std::to_string(idx)+" frame reliability = " << reliablity_value << std::endl;
    
    return true;
}

//====================
